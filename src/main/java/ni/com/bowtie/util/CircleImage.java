package ni.com.bowtie.util;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import static org.opencv.imgproc.Imgproc.COLOR_BGR2GRAY;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
public class CircleImage {
    public void run(String[] args) {
//        String default_file = getClass().getResource("/fxml/icons/smarties.png").getPath();
        String default_file = "D:\\Desarrollo\\BubbleViewer-JAVA\\target\\classes\\fxml\\icons\\smarties2.png";
        String filename = (default_file);
        // Load an image
        Mat src = Imgcodecs.imread(filename);

        Mat gray = new Mat();
        Imgproc.cvtColor(src, gray, COLOR_BGR2GRAY);
        Imgproc.medianBlur(gray, gray, 5);
        Mat circles = new Mat();

        Imgproc.HoughCircles(gray, circles, Imgproc.HOUGH_GRADIENT, 1.0,
                (double) gray.rows() / 16,
                100.0, 30.0, 1, 30);


        for (int x = 0; x < circles.cols(); x++) {

            double[] c = circles.get(0, x);

            Point center = new Point(Math.round(c[0]), Math.round(c[1]));

            int radius = (int) Math.round(c[2]);

            Imgproc.pointPolygonTest(new MatOfPoint2f(), center, true);
        }

        System.exit(0);
    }
}


