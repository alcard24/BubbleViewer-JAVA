package ni.com.bowtie.util;

import nu.pattern.OpenCV;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
public class First_code {

    public static void main(String[] args) {
        OpenCV.loadLocally();
        Mat mat = Mat.eye(3, 3, CvType.CV_8UC1);
        System.out.println("mat = " + mat.dump());
    }

}

