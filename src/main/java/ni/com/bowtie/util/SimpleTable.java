package ni.com.bowtie.util;


import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.test.annotations.type.SampleTest;
import org.junit.experimental.categories.Category;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
@Category(SampleTest.class)
public class SimpleTable {


    public void manipulatePdf(String dest, String titulo, int numeroDePreguntas, int numeroDeRespuestas) throws Exception {
        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(dest));
        Document doc = new Document(pdfDoc);

        Table header = new Table(1);
        header.addCell(titulo).flushContent();

        doc.add(header);


        Table table = new Table(8);
        table.setMargin(10);

        char initial = 'A';

        for (int j = 0; j < numeroDePreguntas; j++) {
            doc.add(new Paragraph(new Text(String.valueOf(j + 1) + " .")));
            for (int i = 0; i < numeroDeRespuestas; i++) {

                Paragraph p = new Paragraph(((char) (initial + i)) + "\n").setFont(PdfFontFactory.createFont(getClass().getResource("/fonts/arialunicodems.ttf").getFile(), PdfEncodings.WINANSI, true));

                Text text = new Text("\uEA3A").setFont(PdfFontFactory.createFont(getClass().getResource("/fonts/SegMDL2.ttf").getFile(), PdfEncodings.IDENTITY_H, true)).setFontSize(21);

                p.add(text);
                Cell cell = new Cell();

                cell.add(p);
                cell.setBorder(Border.NO_BORDER);
                table.addCell(cell);
            }
            doc.add(table);
            table = new Table(8);
        }
        doc.add(table);

        doc.close();
    }

    public void generatePreview() {

    }
}