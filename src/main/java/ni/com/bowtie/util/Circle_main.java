package ni.com.bowtie.util;

import nu.pattern.OpenCV;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
public class Circle_main {
    public static void main(String[] args) {
        // Load the native library.

        OpenCV.loadLocally();
        new CircleImage().run(args);

    }
}
