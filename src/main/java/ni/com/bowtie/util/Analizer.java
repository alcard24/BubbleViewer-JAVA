package ni.com.bowtie.util;

import nu.pattern.OpenCV;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 *
 */
public class Analizer {

    public static void main(String[] args) {

    }

    /***
     *
     * @param uriImage
     * @param ansewerKey
     */
    public static void evaluateTest(String uriImage, String[] ansewerKey) {

        // Cargamos la libreria de open cv
        OpenCV.loadLocally();

        // Variables..
        Mat escalaDeGrises = new Mat();
        Mat borroso = new Mat();
        Mat contorno = new Mat();
        List<MatOfPoint> contornos = new ArrayList<>();
        List<MatOfPoint> contornosDeRespuesta = new ArrayList<>();
        List<Point> puntos = new ArrayList<>();
        //Procesando la imagen para su anali.
        Mat imagen = Imgcodecs.imread(uriImage,
                Imgcodecs.IMREAD_COLOR);
        Imgproc.cvtColor(imagen, escalaDeGrises,
                Imgproc.COLOR_BGR2GRAY);
        // Esto no se si es necesario.
        Imgproc.GaussianBlur(escalaDeGrises,
                borroso,
                escalaDeGrises.size(),
                5,
                5,
                0);

        Imgproc.Canny(borroso,
                contorno,
                75,
                200);


        Imgproc.findContours(borroso,
                contornos,
                new Mat(),
                Imgproc.RETR_EXTERNAL,
                Imgproc.CHAIN_APPROX_SIMPLE);

        puntos = contornos.get(1).toList();

        puntos = puntos
                .stream()
                .sorted(
                ).collect(Collectors.toList());

        for (MatOfPoint cnt : contornos) {
            Rect rect = Imgproc.boundingRect(cnt);
            BigDecimal arco =
                    BigDecimal.valueOf(rect.width).
                            divide(BigDecimal.valueOf(rect.height), RoundingMode.HALF_UP);
            if (rect.width >= 20 &&
                    rect.height >= 20 &&
                    arco.compareTo(BigDecimal.valueOf(0.9)) >= 0 &&
                    arco.compareTo(BigDecimal.valueOf(1.1)) <= 0
                    ) {
                contornosDeRespuesta.add(cnt);
            }
        }

    }
}
