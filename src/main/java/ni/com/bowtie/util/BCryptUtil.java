package ni.com.bowtie.util;

import com.sun.istack.internal.NotNull;
import org.mindrot.jbcrypt.BCrypt;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/16/2018 11:34 PM
 */
public class BCryptUtil {
    private static String salt = "$2a$10$E9av7JoqPbrGDuYeP.AbXu";

    /**
     * Check Hash
     *
     * @param normalString, Normal String
     * @return String
     */
    public static boolean checkHash(@NotNull String normalString) {
        return BCrypt.checkpw(normalString, salt);
    }

    /**
     * Make Hash
     *
     * @param normalString, Normal String
     * @return String
     */
    public static String makeHash(@NotNull String normalString) {
        return BCrypt.hashpw(normalString, salt);
    }
}
