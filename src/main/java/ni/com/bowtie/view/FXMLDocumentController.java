/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.bowtie.view;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import ni.com.bowtie.view.util.ViewHelper;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 *
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private AnchorPane holderPane;
    @FXML
    private JFXButton btnHome;
    @FXML
    private JFXButton btnPricing;
    @FXML
    private JFXButton btnContacts;
    @FXML
    private JFXButton btnWidgets;
    @FXML
    private JFXButton btnProfile;
    @FXML
    private JFXButton btnAlerts;

    AnchorPane contacts, alerts, pricing, profiles, widgets, controls;
    @FXML
    private JFXButton btnControls;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Load all fxmls in a cache
        try {
            contacts = FXMLLoader.load(getClass().getResource("/fxml/dashboard/Contacts.fxml"));
            alerts = FXMLLoader.load(getClass().getResource("/fxml/dashboard/Alerts.fxml"));
            pricing = FXMLLoader.load(getClass().getResource("/fxml/dashboard/Pricing.fxml"));
            profiles = FXMLLoader.load(getClass().getResource("/fxml/dashboard/Profiles.fxml"));
            widgets = FXMLLoader.load(getClass().getResource("/fxml/dashboard/Widgets.fxml"));
            controls = FXMLLoader.load(getClass().getResource("/fxml/dashboard/Controls.fxml"));
            ViewHelper.setNode(holderPane, pricing);
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //Set selected node to a content holder


    @FXML
    private void switchPricing(ActionEvent event) {
        ViewHelper.setNode(holderPane, pricing);
    }

    @FXML
    private void switchContacts(ActionEvent event) {
        ViewHelper.setNode(holderPane, contacts);
    }

    @FXML
    private void switchWidget(ActionEvent event) {
        ViewHelper.setNode(holderPane, widgets);
    }

    @FXML
    private void switchProfile(ActionEvent event) {
        ViewHelper.setNode(holderPane, profiles);
    }

    @FXML
    private void switchAlert(ActionEvent event) {
        ViewHelper.setNode(holderPane, alerts);
    }

    @FXML
    private void switchControls(ActionEvent event) {
        ViewHelper.setNode(holderPane, controls);
    }

}
