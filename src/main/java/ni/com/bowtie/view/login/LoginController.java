package ni.com.bowtie.view.login;

import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import ni.com.bowtie.businessLogic.service.UserService;
import ni.com.bowtie.businessLogic.serviceImpl.UserServiceImpl;
import ni.com.bowtie.model.entity.User;
import ni.com.bowtie.view.util.AlertHelper;
import ni.com.bowtie.view.util.BaseController;
import ni.com.bowtie.view.util.ViewHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/6/2018 7:51 PM
 */
public class LoginController extends BaseController implements Initializable {

    private final static Logger logger = Logger.getLogger(LoginController.class);

    @FXML
    private ImageView ivLogo;
    @FXML
    private JFXTextField txtUser;
    @FXML
    private JFXPasswordField txtPassword;
    @FXML
    private JFXCheckBox ckbRememberMe;
    @FXML
    private JFXButton btnLogin;

    private UserService userService = new UserServiceImpl();
    private JFXDialog dialog = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resources = resources;

        initComponents();
    }

    /**
     * Init Components
     */
    private void initComponents() {
        initComponentImages();

        initComponentListeners();
    }

    /**
     * Init Component Listeners
     */
    private void initComponentListeners() {

        txtUser.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER) && dialog == null) {

                txtPassword.requestFocus();

            } else if (dialog != null) {

                dialog.close();

                dialog = null;
            }
        });

        txtPassword.setOnKeyPressed(event -> {

            if (event.getCode().equals(KeyCode.ENTER) && dialog == null) {

                btnLogin.fire();

            } else if (dialog != null) {

                dialog.close();

                dialog = null;
            }
        });

        btnLogin.setOnAction(event -> {
            if (!StringUtils.isEmpty(txtUser.getText()) && !StringUtils.isEmpty(txtPassword.getText()) && dialog == null) {

                User user = userService.login(txtUser.getText(), txtPassword.getText(), true);

                if (user != null) {

                    if (ckbRememberMe.isSelected()) {

                        Preferences preferences = Preferences.userRoot();

                        preferences.put("user", user.getUserName());
                        preferences.put("password", user.getPassword());
                    }


                    ViewHelper.showDashboard(getClass(), panel, primaryStage, stackPane, maskerPane, user);

                } else {

                    dialog = AlertHelper.showAlert(stackPane, "El usario o contraseña es incorrecto", AlertHelper.AlertType.ERROR);

                }
            } else if (dialog != null) {

                dialog.close();

                dialog = null;

            } else {

                dialog = AlertHelper.showAlert(stackPane, "Ingrese un usario y contraseña", AlertHelper.AlertType.INFORMATION);

            }
        });

    }

    /**
     * Init Component Images
     */
    private void initComponentImages() {
        ivLogo.setImage(new Image("/fxml/icons/ico.png"));
    }
}
