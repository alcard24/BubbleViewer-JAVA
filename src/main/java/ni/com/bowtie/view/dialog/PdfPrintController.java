package ni.com.bowtie.view.dialog;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import ni.com.bowtie.view.util.BaseController;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/17/2018 3:57 PM
 */
public class PdfPrintController extends BaseController implements Initializable {


    @FXML
    private AnchorPane pnlPreview;
    @FXML
    private JFXButton btnImprimir;

    File file;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    /**
     * Init Components
     */
    public void initComponents() {


        initComponentsListeners();
    }

    private void initComponentsListeners() {

    }

    public void setFile(File file) {
        this.file = file;
    }
}
