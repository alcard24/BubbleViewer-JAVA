package ni.com.bowtie.view.util;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 12/4/2018 1:29 PM
 */
public enum ButtonStyle {
    white("white"),
    info("info"),
    warning("warning"),
    danger("danger"),
    purple("purple");

    private String styleName;

    public String getStyleName() {
        return styleName;
    }

    ButtonStyle(String styleName) {
        this.styleName = styleName;
    }

    private ButtonStyle getButtontype(String styleName) {
        ButtonStyle buttonStyle = ButtonStyle.info;

        for (ButtonStyle b : ButtonStyle.values()) {
            if (b.getStyleName().equalsIgnoreCase(styleName)) {
                return b;
            }
        }

        return buttonStyle;
    }

    @Override
    public String toString() {
        return styleName;
    }
}
