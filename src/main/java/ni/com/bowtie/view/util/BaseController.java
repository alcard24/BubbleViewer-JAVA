package ni.com.bowtie.view.util;

import com.jfoenix.controls.JFXButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.scene.control.Pagination;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import ni.com.bowtie.model.entity.User;
import org.controlsfx.control.MaskerPane;

import java.util.ResourceBundle;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
@SuppressWarnings("all")
public class BaseController {

    protected ResourceBundle resources;

    protected Integer maxRows = 20;

    protected Stage primaryStage;

    protected StackPane stackPane;

    protected AnchorPane panel;

    protected MaskerPane maskerPane;

    protected User user;

//    Methods

    /**
     * Set Pagination
     *
     * @param pagination, Pagination Component
     * @param total,      Total Records
     */
    protected void setPagination(Pagination pagination, Integer total) {
        setPagination(pagination, maxRows, total);
    }

    /**
     * Set Pagination
     *
     * @param pagination, Pagination
     * @param maxRows,    Max Rows
     * @param total,      Total Records
     */
    protected void setPagination(Pagination pagination, Integer maxRows, Integer total) {

        if (total != 0) {

            int page = total / maxRows;

            double result = total % maxRows;

            if (result > 0) {
                page = page + 1;
            }

            pagination.setPageCount(page);

        } else {
            pagination.setPageCount(1);
        }

        pagination.setCurrentPageIndex(0);
    }


    protected JFXButton createOptionButton(FontAwesomeIcon icon) {
        return createOptionButton(icon, JFXButton.ButtonType.FLAT, ButtonStyle.info);
    }

    protected JFXButton createOptionButton(FontAwesomeIcon icon, JFXButton.ButtonType buttonType, ButtonStyle buttonStyle) {

        JFXButton jfxButton = new JFXButton();

        jfxButton.setPrefWidth(40);
        jfxButton.setPrefHeight(26);
        jfxButton.setButtonType(buttonType);
        jfxButton.setGraphic(IconApply.createIcon(icon));
        jfxButton.getStyleClass().add(buttonStyle.getStyleName());

        return jfxButton;
    }


    protected JFXButton createOptionButton(FontAwesomeIcon icon, JFXButton.ButtonType buttonType, ButtonStyle buttonStyle, String message) {

        JFXButton jfxButton = createOptionButton(icon, buttonType, buttonStyle);

        jfxButton.setTooltip(createTooltip(message));

        return jfxButton;
    }


    /**
     * Create Option Butoon Flat
     *
     * @param icon,        Icon
     * @param buttonType,  Button type
     * @param buttonStyle, Button Style
     * @param message,     Message
     * @return
     */
    protected JFXButton createOptionButtonFlat(FontAwesomeIcon icon, String message) {

        JFXButton jfxButton = new JFXButton();


        jfxButton.setButtonType(JFXButton.ButtonType.FLAT);

        jfxButton.setGraphic(IconApply.createIconAccent(icon, "16"));

        jfxButton.setTooltip(createTooltip(message));

        return jfxButton;
    }

    /**
     * Create Tool Tip
     *
     * @param message, Message
     * @return Tooltip
     */
    protected Tooltip createTooltip(String message) {

        return new Tooltip(message);
    }

    //    Getters & Setters

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public StackPane getStackPane() {
        return stackPane;
    }

    public void setStackPane(StackPane stackPane) {
        this.stackPane = stackPane;
    }

    public AnchorPane getPanel() {
        return panel;
    }

    public void setPanel(AnchorPane panel) {
        this.panel = panel;
    }

    public MaskerPane getMaskerPane() {
        return maskerPane;
    }

    public void setMaskerPane(MaskerPane maskerPane) {
        this.maskerPane = maskerPane;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
