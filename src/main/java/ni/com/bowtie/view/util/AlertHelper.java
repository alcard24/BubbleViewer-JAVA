package ni.com.bowtie.view.util;

import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/17/2018 4:57 AM
 */
public class AlertHelper {

    public enum AlertType {
        NONE(""),
        ERROR("delete"),
        CANCEL("cancel"),
        INFORMATION("information");

        private String iconName;

        AlertType(String iconName) {
            this.iconName = iconName;
        }

        public String getIconName() {
            return iconName;
        }
    }


    /**
     * Show Alert
     *
     * @param stackPane, Stack Pane
     * @param message,   Message
     */
    public static void showAlert(StackPane stackPane, String message) {
        showAlert(stackPane, message, AlertType.NONE);
    }


    /**
     * Show Alert
     *
     * @param stackPane, Stack Pane
     * @param message,   Message
     * @param alertType, Alert Type
     */
    public static JFXDialog showAlert(StackPane stackPane, String message, AlertType alertType) {

        JFXDialogLayout jfxDialogLayout = new JFXDialogLayout();

        VBox hBox = new VBox();

        hBox.setSpacing(5);
        hBox.setAlignment(Pos.TOP_CENTER);

        if (alertType != AlertType.NONE) {

            ImageView imageView = new ImageView(new Image("/colorIcons/".concat(alertType.iconName).concat(".png")));

            imageView.setFitHeight(64);
            imageView.setFitWidth(64);

            hBox.getChildren().add(imageView);

        }

        Label label = new Label();

        label.setText(message);
        hBox.getChildren().add(label);

        jfxDialogLayout.setBody(hBox);

        JFXDialog jfxDialog = new JFXDialog(stackPane, jfxDialogLayout, JFXDialog.DialogTransition.CENTER);

        jfxDialog.show();

        return jfxDialog;

    }
}
