package ni.com.bowtie.view.util;

import com.jfoenix.controls.JFXCheckBox;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/18/2018 8:34 AM
 */
public class CheckBoxCell<T> extends TableCell<T, Boolean> {
    private JFXCheckBox checkBox;
    private Pos alignment = Pos.CENTER_LEFT;

    public CheckBoxCell(Pos alignment) {
        this.alignment = alignment;
        instanceGraphic();
    }

    public CheckBoxCell() {
        instanceGraphic();
    }

    /**
     * Instance Graphic
     */
    private void instanceGraphic() {
        checkBox = new JFXCheckBox();
        checkBox.setDisable(true);
        checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (isEditing())
                commitEdit(newValue == null ? false : newValue);
        });
    }

    @Override
    public void startEdit() {

        if (isEmpty() && isEditable()) {
            return;
        }
        super.startEdit();

        checkBox.setDisable(false);
        checkBox.requestFocus();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        checkBox.setDisable(true);
    }

    public void commitEdit(Boolean value) {
        super.commitEdit(value);
        checkBox.setDisable(true);
    }

    @Override
    public void updateItem(Boolean item, boolean empty) {
        super.updateItem(item, empty);
        if (!isEmpty()) {

            setGraphic(checkBox);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            checkBox.setSelected(item);
            setAlignment(alignment);
        } else {
            setGraphic(null);
        }
    }
}
