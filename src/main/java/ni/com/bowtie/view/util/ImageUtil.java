package ni.com.bowtie.view.util;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/17/2018 11:38 PM
 */
public class ImageUtil {

    private static final Logger logger = Logger.getLogger(ImageUtil.class);

    /**
     * Retrieve Image
     *
     * @param imageByte, byte[]
     * @return Image
     */
    public static Image retrieveImage(byte[] imageByte) {
        ByteArrayInputStream in = new ByteArrayInputStream(imageByte);

        Image image = new Image(in);

        try {
            in.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        return image;
    }


    /**
     * Retrieve Array Image
     *
     * @param image, Image
     * @return byte[]
     */
    public static byte[] retrieveArrayImage(Image image) {
        BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        byte[] arrayByte = null;

        try {
            ImageIO.write(bufferedImage, "png", byteArrayOutputStream);

            arrayByte = byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        } finally {
            try {
                byteArrayOutputStream.close();
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }

        return arrayByte;
    }
}
