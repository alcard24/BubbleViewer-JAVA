package ni.com.bowtie.view.util;

/**
 * <p>
 * Bow Tie Product 2017 - 2019
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 4/7/2019 9:17 PM
 */
public enum Catalogue {
    TestType("01"),
    DefaultValues("02"),
    TestTheme("03"),
    RevisionType("04");

    private String value;

    Catalogue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
