package ni.com.bowtie.view.util;

import javafx.animation.FadeTransition;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import ni.com.bowtie.model.entity.User;
import ni.com.bowtie.view.DashboardController;
import ni.com.bowtie.view.login.LoginController;
import org.controlsfx.control.MaskerPane;

import java.io.IOException;
import java.net.URL;

/**
 * ]
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
public class ViewHelper {

    /**
     * Set Node
     *
     * @param holderPane, Holder Pane
     * @param node,       Node
     */
    public static void setNode(AnchorPane holderPane, Node node) {
        holderPane.getChildren().clear();
        holderPane.getChildren().add(node);

        FadeTransition ft = new FadeTransition(Duration.millis(1500));

        ft.setNode(node);
        ft.setFromValue(0.1);
        ft.setToValue(1);
        ft.setCycleCount(1);
        ft.setAutoReverse(false);
        ft.play();

        AnchorPane.setTopAnchor(node, 0d);
        AnchorPane.setLeftAnchor(node, 0d);
        AnchorPane.setRightAnchor(node, 0d);
        AnchorPane.setBottomAnchor(node, 0d);

    }

    /**
     * Show Dashboard
     *
     * @param c,            Class
     * @param panel,        Panel
     * @param primaryStage, Primary Stage
     * @param stackPane,    Stack Pane
     * @param maskerPane,   Masker Pane
     * @param user,         User
     */
    public static void showDashboard(Class c, AnchorPane panel, Stage primaryStage, StackPane stackPane, MaskerPane maskerPane, User user) {
        try {

            FXMLLoader fxmlLoader = new FXMLLoader();

            fxmlLoader.setLocation(c.getResource("/fxml/dashboard/Dashboard.fxml"));

            AnchorPane dashboard = fxmlLoader.load();


            DashboardController dashboardController = fxmlLoader.getController();

            dashboardController.setPanel(panel);
            dashboardController.setPrimaryStage(primaryStage);
            dashboardController.setMaskerPane(maskerPane);
            dashboardController.setStackPane(stackPane);
            dashboardController.setUser(user);

            dashboardController.initComponents();

            ViewHelper.setNode(panel, dashboard);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showLogin(Class c, AnchorPane panel, Stage primaryStage, StackPane stackPane, MaskerPane maskerPane) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();

            fxmlLoader.setLocation(c.getResource("/fxml/login/Login.fxml"));

            AnchorPane login = fxmlLoader.load();

            LoginController loginController = fxmlLoader.getController();

            loginController.setPanel(panel);
            loginController.setPrimaryStage(primaryStage);
            loginController.setMaskerPane(maskerPane);
            loginController.setStackPane(stackPane);

            ViewHelper.setNode(panel, login);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show Partial
     *
     * @param resource,     Resource
     * @param holderPane,   Holder Panel
     * @param panel,        Panel
     * @param primaryStage, Primary Stage
     * @param stackPane,    Stack Pane
     * @param maskerPane,   Masker Pane
     * @param changeView,   Change View
     * @param user,         User
     */
    public static void showPartial(URL resource, AnchorPane holderPane, AnchorPane panel, Stage primaryStage, StackPane stackPane, MaskerPane maskerPane, ChangeView changeView, User user) {

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();

            fxmlLoader.setLocation(resource);

            AnchorPane genericPanel = fxmlLoader.load();

            BaseController baseController = fxmlLoader.getController();

            changeView.beforeDefaultSet(baseController);

            baseController.setPanel(panel);
            baseController.setPrimaryStage(primaryStage);
            baseController.setMaskerPane(maskerPane);
            baseController.setStackPane(stackPane);
            baseController.setUser(user);

            changeView.afterDefaultSet(baseController);

            ViewHelper.setNode(holderPane, genericPanel);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show Partial
     *
     * @param resource,     Resource
     * @param holderPane,   Holder Pane
     * @param panel,        Panel
     * @param primaryStage, Primary Stage
     * @param stackPane,    Stack Pane
     * @param maskerPane,   Masker Pane
     * @param user,         User
     */
    public static void showPartial(URL resource, AnchorPane holderPane, AnchorPane panel, Stage primaryStage, StackPane stackPane, MaskerPane maskerPane, User user) {

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();

            fxmlLoader.setLocation(resource);

            AnchorPane genericPanel = fxmlLoader.load();

            BaseController baseController = fxmlLoader.getController();

            baseController.setPanel(panel);
            baseController.setPrimaryStage(primaryStage);
            baseController.setMaskerPane(maskerPane);
            baseController.setStackPane(stackPane);
            baseController.setUser(user);

            ViewHelper.setNode(holderPane, genericPanel);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
