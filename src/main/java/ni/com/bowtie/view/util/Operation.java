package ni.com.bowtie.view.util;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/10/2018 9:25 AM
 */
public enum Operation {
    NONE,
    READ_ONLY,
    EDIT,
    CREATE
}
