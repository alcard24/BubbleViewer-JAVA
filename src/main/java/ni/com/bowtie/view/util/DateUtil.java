package ni.com.bowtie.view.util;

import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.LocalDateTime;

import java.sql.Timestamp;


/**
 * <p>
 * Bow Tie Product 2017 - 2019
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 2/2/2019 11:37 PM
 */
public class DateUtil {


    public static String diffForHumans(Timestamp timestamp) {

        String predicate = "Hace ";

        LocalDateTime now = LocalDateTime.now();
        LocalDateTime comparedDate = new LocalDateTime(timestamp);


        Period deference = new Period(comparedDate, now, PeriodType.yearMonthDayTime());

        int year = deference.getYears();
        int month = deference.getMonths();
        int day = deference.getDays();
        int hour = deference.getHours();
        int minute = deference.getMinutes();

        if (month > 12) {

            predicate = predicate.concat(String.valueOf(year)).concat(year > 1 ? " años" : " año");

        } else if (month > 0) {

            predicate = predicate.concat(String.valueOf(month)).concat(month > 1 ? " meses" : " mes");

        } else if (day > 0) {

            predicate = predicate.concat(String.valueOf(day)).concat(day > 1 ? " días" : " día");

        } else if (hour > 0) {

            predicate = predicate.concat(String.valueOf(hour)).concat(hour > 1 ? " horas" : " hora");

        } else if (minute > 0) {

            predicate = predicate.concat(String.valueOf(minute)).concat(minute > 1 ? " minutos" : " minuto");

        } else {
            predicate = predicate.concat(" un momento");
        }

        return predicate;
    }

}
