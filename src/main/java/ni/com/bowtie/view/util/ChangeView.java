package ni.com.bowtie.view.util;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/17/2018 3:12 PM
 */
public interface ChangeView {

    /**
     * After Default Set
     *
     * @param baseController, Base Controller
     */
    void afterDefaultSet(BaseController baseController);

    /**
     * Before Default Set
     *
     * @param baseController, Base Controller
     */
    void beforeDefaultSet(BaseController baseController);
}
