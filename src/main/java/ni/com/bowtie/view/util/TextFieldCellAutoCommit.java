package ni.com.bowtie.view.util;

import com.jfoenix.controls.JFXTextField;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/18/2018 7:46 AM
 */
public class TextFieldCellAutoCommit<T> extends TableCell<T, String> {

    private TextField textField;

    private TextFieldCharSet textFieldChartSet = TextFieldCharSet.NONE;

    private String style = "";

    public TextFieldCellAutoCommit() {
    }

    public TextFieldCellAutoCommit(TextFieldCharSet textFieldChartSet, String style) {
        this.textFieldChartSet = textFieldChartSet;
        this.style = style;
    }


    @Override
    public void startEdit() {
        if (!isEmpty() && isEditable()) {
            super.startEdit();
            createTextField();
            setText(null);
            setGraphic(textField);
            textField.selectAll();
        }
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();

        setText(getItem());
        setGraphic(null);
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (textField != null) {
                    textField.setText(getString());
                }
                setText(null);
                setGraphic(textField);
            } else {
                setText(getString());
                setGraphic(null);
            }
        }

        setStyle(style);
    }

    private void createTextField() {
        textField = new TextField(getString());

        if (textFieldChartSet != TextFieldCharSet.NONE) {
            Pattern pattern = Pattern.compile(textFieldChartSet.getText());

            TextFormatter formatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
                return pattern.matcher(change.getControlNewText()).matches() ? change : null;
            });

            textField.setTextFormatter(formatter);
        }

        textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);

        textField.setOnAction(event -> {
            commitEdit(textField.getText());
        });

        textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                commitEdit(textField.getText());
            }
        });
    }

    private String getString() {
        return getItem() == null ? "" : getItem().toString();
    }
}
