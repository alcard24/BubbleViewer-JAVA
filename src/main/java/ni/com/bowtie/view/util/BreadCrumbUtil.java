package ni.com.bowtie.view.util;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.util.ArrayList;
import java.util.List;

/**
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 10/3/2018 10:06 PM
 */
public class BreadCrumbUtil {

    /**
     * Add HyperLink
     *
     * @param panelHeader, Panel Header
     * @param caption,     Caption
     * @param separate,    Separate
     */
    public static void addHyperLink(HBox panelHeader, String caption, boolean separate) {
        if (separate) {
            Label separator = new Label("/");

            panelHeader.getChildren().add(separator);
        }

        Hyperlink hyperLink = new Hyperlink(caption);

        panelHeader.getChildren().add(hyperLink);
    }

    /**
     * Add HyperLink
     *
     * @param panelHeader, Panel Header
     * @param caption,     Caption
     * @param separate,    Separate
     * @param event,       Event
     */
    public static void addHyperLink(HBox panelHeader, String caption, boolean separate, EventHandler<ActionEvent> event) {
        ObservableList<Node> olNode = panelHeader.getChildren();

        boolean found = false;

        for (int i = olNode.size() - 1; i >= 0; i--) {
            Node node = olNode.get(i);

            if (node instanceof Hyperlink) {
                Hyperlink hyperlink = (Hyperlink) node;

                if (hyperlink.getText().equals(caption)) {
                    found = true;

                    break;
                }
            }
        }

        if (!found) {
            if (separate) {
                Label separator = new Label("/");

                panelHeader.getChildren().add(separator);
            }

            Hyperlink hyperLink = new Hyperlink(caption);

            hyperLink.setOnAction(event);

            panelHeader.getChildren().add(hyperLink);
        }
    }


    /**
     * Remove Before Hyper Link
     */
    public static void removeBeforeHyperLink(HBox panelHeaderSource, String caption) {
        boolean goToDelete = false;

        List<Node> forDelete = new ArrayList<>();

        for (Node node : panelHeaderSource.getChildren()) {
            if (goToDelete) {

                forDelete.add(node);

            } else if (node instanceof Hyperlink) {
                Hyperlink hyperlink = (Hyperlink) node;

                if (hyperlink.getText().trim().equalsIgnoreCase(caption)) {
                    goToDelete = true;
                }
            }
        }

        panelHeaderSource.getChildren().removeAll(forDelete);
    }

    /**
     * Define Panel Header
     *
     * @param panelHeaderSource, Panel Header Source
     * @param panelHeaderTarget, Panel Header Target
     */
    public static void definePanelHeader(HBox panelHeaderSource, HBox panelHeaderTarget) {
        ObservableList<Node> olNodeSource = panelHeaderSource.getChildren();
        ObservableList<Node> olNodeClon = FXCollections.observableArrayList(olNodeSource);

        panelHeaderTarget.getChildren().addAll(olNodeClon);
    }

    /**
     * Clear Panel
     *
     * @param panelHeader, Panel Header
     */
    public static void clearPanel(HBox panelHeader) {
        panelHeader.getChildren().clear();
    }

    /**
     * Retrieve Last Hyper Link
     *
     * @param navigation, Navigation
     */
    public static Hyperlink retrieveLastHyperLink(HBox navigation) {
        int size = navigation.getChildren().size();
        if (size > 1) {
            return (Hyperlink) navigation.getChildren().get(size - 2);
        } else {
            return (Hyperlink) navigation.getChildren().get(size - 1);
        }
    }
}
