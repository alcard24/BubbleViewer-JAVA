package ni.com.bowtie.view.util;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.stage.Stage;
import ni.com.bowtie.businessLogic.service.AnswerService;
import ni.com.bowtie.businessLogic.service.QuestionService;
import ni.com.bowtie.businessLogic.serviceImpl.AnswerServiceImpl;
import ni.com.bowtie.businessLogic.serviceImpl.QuestionServiceImpl;
import ni.com.bowtie.model.entity.Answer;
import ni.com.bowtie.model.entity.Question;
import ni.com.bowtie.model.entity.Test;
import org.controlsfx.control.MaskerPane;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/18/2018 9:03 AM
 */
public class PdfProcess {


    public static void generatePreview(Class c, Stage stage, Test test, MaskerPane maskerPane) throws IOException {

        maskerPane.setVisible(true);

        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                File file = File.createTempFile("preview", ".pdf");


                QuestionService questionService = new QuestionServiceImpl();
                AnswerService answerService = new AnswerServiceImpl();

                PdfDocument pdfDoc = new PdfDocument(new PdfWriter(file));
                Document doc = new Document(pdfDoc);

                Table header = new Table(1);
                header.addCell(test.getName()).flushContent();

                doc.add(header);

                List<Question> lstQuestion = questionService.findAllQuestionByTest(test);

                Table table = new Table(8);
                table.setMargin(10);


                for (Question question : lstQuestion) {

                    doc.add(new Paragraph(new Text(question.getBody())));

                    List<Answer> lstAnswer = answerService.findAllAnswerByQuestion(question);

                    for (Answer answer : lstAnswer) {

                        Paragraph p = new Paragraph(answer.getBody() + "\n").setFont(PdfFontFactory.createFont(c.getResource("/fonts/arialunicodems.ttf").getFile(), PdfEncodings.WINANSI, true));

                        Text text = new Text("\uEA3A").setFont(PdfFontFactory.createFont(c.getResource("/fonts/SegMDL2.ttf").getFile(), PdfEncodings.IDENTITY_H, true)).setFontSize(21);

                        p.add(text);
                        Cell cell = new Cell();

                        cell.add(p);
                        cell.setBorder(Border.NO_BORDER);
                        table.addCell(cell);
                    }
                    doc.add(table);
                    table = new Table(8);
                }


                doc.close();

                Platform.runLater(() -> {
                    RunDialog.previewPDF(c, stage, file);
                });

                return null;
            }

            @Override
            protected void succeeded() {
                maskerPane.setVisible(false);
            }
        };

        new Thread(task).start();
    }
}
