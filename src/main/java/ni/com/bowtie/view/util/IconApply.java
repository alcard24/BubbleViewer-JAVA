package ni.com.bowtie.view.util;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
public class IconApply {

    /**
     * Create Icon
     *
     * @param fontAwesomeIcon, Font Awesome Icon
     * @return Label, Icon.
     */
    public static FontAwesomeIconView createIcon(FontAwesomeIcon fontAwesomeIcon) {

        FontAwesomeIconView fontAwesomeIconView = new FontAwesomeIconView();

        fontAwesomeIconView.setText(fontAwesomeIcon.characterToString());
        fontAwesomeIconView.setStyle("-fx-fill: white;");
        fontAwesomeIconView.setSize("12");

        return fontAwesomeIconView;
    }

    /**
     * Create Icon
     *
     * @param fontAwesomeIcon, Font Awesome Icon
     * @param size,            Size
     * @return Label, Icon.
     */
    public static FontAwesomeIconView createIcon(FontAwesomeIcon fontAwesomeIcon, String size) {

        FontAwesomeIconView fontAwesomeIconView = new FontAwesomeIconView();

        fontAwesomeIconView.setSize(size);

        fontAwesomeIconView.setText(fontAwesomeIcon.characterToString());
        fontAwesomeIconView.setStyle("-fx-fill: white;");


        return fontAwesomeIconView;
    }

    /**
     * Create Icon Dark
     *
     * @param fontAwesomeIcon, Font Awesome Icon
     * @param size,            Size
     * @return Label, Icon.
     */
    public static FontAwesomeIconView createIconDark(FontAwesomeIcon fontAwesomeIcon, String size) {

        FontAwesomeIconView fontAwesomeIconView = new FontAwesomeIconView();

        fontAwesomeIconView.setSize(size);

        fontAwesomeIconView.setText(fontAwesomeIcon.characterToString());
        fontAwesomeIconView.setStyle("-fx-fill: black;");


        return fontAwesomeIconView;
    }

    /**
     * Create Icon Dark
     *
     * @param fontAwesomeIcon, Font Awesome Icon
     * @param size,            Size
     * @return Label, Icon.
     */
    public static FontAwesomeIconView createIconAccent(FontAwesomeIcon fontAwesomeIcon, String size) {

        FontAwesomeIconView fontAwesomeIconView = new FontAwesomeIconView();

        fontAwesomeIconView.setSize(size);

        fontAwesomeIconView.setText(fontAwesomeIcon.characterToString());
        fontAwesomeIconView.setStyle("-fx-fill: #133782;");


        return fontAwesomeIconView;
    }

}
