package ni.com.bowtie.view.util;


import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ni.com.bowtie.view.about.SupportDialog;
import ni.com.bowtie.view.dialog.PdfPrintController;
import ni.com.bowtie.view.dialog.PdfViewerController;
import org.apache.log4j.Logger;

import java.io.File;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/16/2018 11:53 PM
 */
public class RunDialog {

    private final static Logger logger = Logger.getLogger(RunDialog.class);


    /**
     * Preview PDF FILE
     *
     * @param stage, Stage
     * @param file,  File
     */
    public static void previewPDF(Class c, Stage stage, File file) {
        try {
            Image image = new Image("/fxml/icons/ico.png");

            FXMLLoader fxmlLoader = new FXMLLoader();

            fxmlLoader.setLocation(c.getResource("/fxml/dialog/PdfPreview.fxml"));

            Stage dialogStage = new Stage();
            dialogStage.getIcons().addAll(image);
            dialogStage.setTitle("Preview PDF");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initStyle(StageStyle.DECORATED);
            dialogStage.initOwner(stage);

            AnchorPane borderPane = fxmlLoader.load();
            PdfViewerController pdfViewerController = fxmlLoader.getController();

            Scene scene = new Scene(borderPane, 640, 480);
            scene.getStylesheets().add("/css/style.css");

            pdfViewerController.initComponents();
            pdfViewerController.setPrimaryStage(dialogStage);

            pdfViewerController.setFile(file);
            pdfViewerController.loadFile();

            dialogStage.setScene(scene);
            dialogStage.setResizable(false);
            dialogStage.showAndWait();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }

    /**
     * Pdf Print Dialog
     */
    public static void pdfPrintDialog(Class c, Stage stage, File file) {
        try {
            Image image = new Image("/fxml/icons/ico.png");

            FXMLLoader fxmlLoader = new FXMLLoader();

            fxmlLoader.setLocation(c.getResource("/fxml/dialog/PrinterDialog.fxml"));

            Stage dialogStage = new Stage();
            dialogStage.getIcons().addAll(image);
            dialogStage.setTitle("Print PDF");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initStyle(StageStyle.DECORATED);
            dialogStage.initOwner(stage);

            AnchorPane borderPane = fxmlLoader.load();
            PdfPrintController pdfPrintController = fxmlLoader.getController();

            Scene scene = new Scene(borderPane, 800, 720);
            scene.getStylesheets().add("/css/style.css");

            pdfPrintController.setPrimaryStage(dialogStage);

            pdfPrintController.setFile(file);

            pdfPrintController.initComponents();

            dialogStage.setScene(scene);
            dialogStage.setResizable(false);
            dialogStage.showAndWait();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }


    /**
     * Pdf Print Dialog
     */
    public static void supportDialog(Class c, Stage stage) {
        try {
            Image image = new Image("/fxml/icons/ico.png");

            FXMLLoader fxmlLoader = new FXMLLoader();

            fxmlLoader.setLocation(c.getResource("/fxml/dashboard/about/Support.fxml"));

            Stage dialogStage = new Stage();
            dialogStage.getIcons().addAll(image);
            dialogStage.setTitle("Soporte");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initStyle(StageStyle.DECORATED);
            dialogStage.initOwner(stage);

            AnchorPane borderPane = fxmlLoader.load();
            SupportDialog pdfPrintController = fxmlLoader.getController();

            Scene scene = new Scene(borderPane, 500, 300);
            scene.getStylesheets().add("/css/style.css");

            pdfPrintController.setPrimaryStage(dialogStage);

            dialogStage.setScene(scene);
            dialogStage.setResizable(false);
            dialogStage.showAndWait();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
