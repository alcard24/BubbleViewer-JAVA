package ni.com.bowtie.view.util;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/18/2018 7:47 AM
 */
public enum TextFieldCharSet {
    DOUBLE_INPUT("\\d*|\\d+\\.\\d*"),
    INTEGER_INPUT("\\d+"),
    NONE("");

    TextFieldCharSet(String text) {
        this.text = text;
    }

    private String text;

    public String getText() {
        return text;
    }
}
