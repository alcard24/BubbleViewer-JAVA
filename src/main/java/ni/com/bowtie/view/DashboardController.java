package ni.com.bowtie.view;

import com.jfoenix.controls.JFXButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import ni.com.bowtie.view.create.CreateMainController;
import ni.com.bowtie.view.evaluate.EvaluateController;
import ni.com.bowtie.view.evaluate.EvaluateMainController;
import ni.com.bowtie.view.util.BaseController;
import ni.com.bowtie.view.util.IconApply;
import ni.com.bowtie.view.util.RunDialog;
import ni.com.bowtie.view.util.ViewHelper;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
public class DashboardController extends BaseController implements Initializable {

    private Logger logger = Logger.getLogger(DashboardController.class);

    @FXML
    private AnchorPane holderPane;
    @FXML
    private JFXButton btnCrear;
    @FXML
    private Label txtUserName;
    @FXML
    private JFXButton btnRevisar;
    @FXML
    private JFXButton btnPeople;
    @FXML
    private JFXButton btnEstadisticas;
    @FXML
    private JFXButton btnAcercaDe;
    @FXML
    private JFXButton btnConfiguracion;

    private ContextMenu currentPopup = null;

    private JFXButton selectedButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resources = resources;

    }

    /**
     * Init Components
     */
    public void initComponents() {
        try {

            viewCreateMain();

            initComponentListeners();

            txtUserName.setText(user.getUserName().toUpperCase());

            setSelectedButton(btnCrear);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void initComponentListeners() {


        ContextMenu examenMenu = new ContextMenu();

        MenuItem createItem = new MenuItem("Examenes", IconApply.createIconDark(FontAwesomeIcon.PAPER_PLANE, "16"));

        createItem.setOnAction(event -> {
            setSelectedButton(btnCrear);
            viewCreateMain();
        });

        MenuItem themeItem = new MenuItem("Temas", IconApply.createIconDark(FontAwesomeIcon.KEY, "16"));

        themeItem.setOnAction(event -> {
            setSelectedButton(btnCrear);
        });

        examenMenu.getItems().addAll(createItem, themeItem);

        btnCrear.setOnAction(event -> {

            if (currentPopup != null) {

                currentPopup.hide();

            }

            examenMenu.show(btnCrear, this.getPrimaryStage().getX() + btnCrear.getLayoutX() + btnCrear.getWidth(), this.primaryStage.getY() + btnCrear.getLayoutY() + btnCrear.getHeight());

            currentPopup = examenMenu;
        });

        ContextMenu peopleMenu = new ContextMenu();

        MenuItem groupItem = new MenuItem("Grupos", IconApply.createIconDark(FontAwesomeIcon.GROUP, "16"));

        groupItem.setOnAction(event -> {
        });

        MenuItem studentItem = new MenuItem("Estudiantes", IconApply.createIconDark(FontAwesomeIcon.BOOK, "16"));

        studentItem.setOnAction(event -> {

        });

        peopleMenu.getItems().addAll(groupItem, studentItem);

        btnRevisar.setOnAction(event -> {
            viewCreateEvaluate();
            setSelectedButton(btnRevisar);
        });


        ContextMenu aboutMenu = new ContextMenu();

        MenuItem supportItem = new MenuItem("Soporte", IconApply.createIconDark(FontAwesomeIcon.SUPPORT, "16"));


        MenuItem softwareInformationItem = new MenuItem("Información del software", IconApply.createIconDark(FontAwesomeIcon.LAPTOP, "16"));

        MenuItem userManualItem = new MenuItem("Manual de usuario", IconApply.createIconDark(FontAwesomeIcon.BOOK, "16"));


        softwareInformationItem.setOnAction(event -> {


        });

        supportItem.setOnAction(event -> {

            RunDialog.supportDialog(this.getClass(), this.getPrimaryStage());

        });

        aboutMenu.getItems().addAll(supportItem, softwareInformationItem);

        btnPeople.setOnAction(event -> {

            if (currentPopup != null) {

                currentPopup.hide();

            }

            peopleMenu.show(btnPeople, this.getPrimaryStage().getX() + btnPeople.getLayoutX() + btnPeople.getWidth(), this.primaryStage.getY() + btnPeople.getLayoutY() + btnPeople.getHeight());

            currentPopup = peopleMenu;
        });

        btnEstadisticas.setOnAction(event -> {
        });

        btnConfiguracion.setOnAction(event -> {

        });

        btnAcercaDe.setOnAction(event -> {

//            ViewHelper.showPartial(getClass().getResource("/fxml/dashboard/about/AboutMain.fxml"), holderPane, panel, primaryStage, stackPane, maskerPane, this.getUser());

            if (currentPopup != null) {

                currentPopup.hide();

            }

            aboutMenu.show(btnPeople, this.getPrimaryStage().getX() + btnAcercaDe.getLayoutX() + btnAcercaDe.getWidth(), this.primaryStage.getY() + btnAcercaDe.getLayoutY() + btnAcercaDe.getHeight());

            currentPopup = aboutMenu;

        });
    }

    @FXML
    public void createClick(ActionEvent actionEvent) {
        viewCreateMain();
    }

    @FXML
    public void btnSignOut_onAction(ActionEvent event) {

        Preferences preferences = Preferences.userRoot();

        preferences.remove("user");
        preferences.remove("password");

        ViewHelper.showLogin(getClass(), panel, primaryStage, stackPane, maskerPane);
    }

    /**
     * View Create Main
     */
    private void viewCreateMain() {
        try {

            FXMLLoader fxmlLoader = new FXMLLoader();

            fxmlLoader.setLocation(getClass().getResource("/fxml/dashboard/create/CreateMain.fxml"));

            AnchorPane createMain = fxmlLoader.load();

            CreateMainController createMainController = fxmlLoader.getController();

            createMainController.setPanel(holderPane);
            createMainController.setPrimaryStage(primaryStage);
            createMainController.setMaskerPane(maskerPane);
            createMainController.setStackPane(stackPane);

            ViewHelper.setNode(holderPane, createMain);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * View Create Main
     */
    private void viewCreateEvaluate() {
        try {

            FXMLLoader fxmlLoader = new FXMLLoader();

            fxmlLoader.setLocation(getClass().getResource("/fxml/dashboard/evaluate/Evaluate.fxml"));

            AnchorPane createMain = fxmlLoader.load();

            EvaluateController evaluateController = fxmlLoader.getController();

            evaluateController.setPanel(holderPane);
            evaluateController.setPrimaryStage(primaryStage);
            evaluateController.setMaskerPane(maskerPane);
            evaluateController.setStackPane(stackPane);

            ViewHelper.setNode(holderPane, createMain);

//            FXMLLoader fxmlLoader = new FXMLLoader();
//
//            fxmlLoader.setLocation(getClass().getResource("/fxml/dashboard/evaluate/EvaluateMain.fxml"));
//
//            AnchorPane createMain = fxmlLoader.load();
//
//            EvaluateMainController createMainController = fxmlLoader.getController();
//
//            createMainController.setPanel(holderPane);
//            createMainController.setPrimaryStage(primaryStage);
//            createMainController.setMaskerPane(maskerPane);
//            createMainController.setStackPane(stackPane);
//
//            ViewHelper.setNode(holderPane, createMain);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    private void setSelectedButton(JFXButton selectedButton) {

        if (this.selectedButton != null) {
            this.selectedButton.setStyle("-fx-alignment: left; -fx-background-color: #222D32;");
        }

        this.selectedButton = selectedButton;

        this.selectedButton.setStyle("-fx-alignment: left; -fx-background-color: #2A3F54;");
    }
}
