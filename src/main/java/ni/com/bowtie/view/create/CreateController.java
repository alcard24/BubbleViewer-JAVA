package ni.com.bowtie.view.create;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import ni.com.bowtie.businessLogic.service.AnswerService;
import ni.com.bowtie.businessLogic.service.CatalogueService;
import ni.com.bowtie.businessLogic.service.QuestionService;
import ni.com.bowtie.businessLogic.service.TestService;
import ni.com.bowtie.businessLogic.serviceImpl.AnswerServiceImpl;
import ni.com.bowtie.businessLogic.serviceImpl.CatalogueServiceImpl;
import ni.com.bowtie.businessLogic.serviceImpl.QuestionServiceImpl;
import ni.com.bowtie.businessLogic.serviceImpl.TestServiceImpl;
import ni.com.bowtie.model.entity.Answer;
import ni.com.bowtie.model.entity.Question;
import ni.com.bowtie.model.entity.Test;
import ni.com.bowtie.model.util.DAOException;
import ni.com.bowtie.view.util.*;

import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/17/2018 6:14 PM
 */
public class CreateController extends BaseController implements Initializable {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private BorderPane borderPane;
    @FXML
    private ImageView ivLogo;
    @FXML
    private JFXButton btnUpload;
    @FXML
    private JFXTextField txtName;
    @FXML
    private JFXTextField txtSubject;
    @FXML
    private JFXTextField txtVal;
    @FXML
    private JFXDatePicker dpPlanDate;
    @FXML
    private JFXTabPane tab;
    @FXML
    private JFXTextField txtQuestionBody;
    @FXML
    private JFXTextField txtQuestionNumber;
    @FXML
    private JFXTextField txtQuestionVal;
    @FXML
    private JFXButton btnAddQuestion;
    @FXML
    private TableView<Question> tblQuestion;
    @FXML
    private TableColumn<Question, String> questionBodyColumn;
    @FXML
    private TableColumn<Question, String> valQuestionColumn;
    @FXML
    private TableColumn<Question, String> actionQuestionColumn;
    @FXML
    private TableView<Answer> tblAnswer;
    @FXML
    private TableColumn<Answer, String> bodyAnswerColumn;
    @FXML
    private TableColumn<Answer, Boolean> checkAnswerColumn;
    @FXML
    private TableColumn<Answer, String> actionAnsweColumn;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnPreview;

    private HBox navigation;
    private Operation operation = Operation.READ_ONLY;
    private Test test;
    private Hyperlink hyperlink;

    private TestService testService = new TestServiceImpl();
    private CatalogueService catalogueService = new CatalogueServiceImpl();
    private QuestionService questionService = new QuestionServiceImpl();
    private AnswerService answerService = new AnswerServiceImpl();

    private ObservableList<Question> olQuestion;
    private List<AnswersOfQuestion> answersOfQuestionList;

    private byte[] img = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Init Component Values
     */
    public void initComponents() {

        if (operation == Operation.CREATE) {
            olQuestion = FXCollections.observableArrayList();

            tblQuestion.setItems(olQuestion);

            answersOfQuestionList = new ArrayList<>();
        } else if (operation == Operation.EDIT) {

            olQuestion = FXCollections.observableArrayList(questionService.findAllQuestionByTest(test));

            tblQuestion.setItems(olQuestion);
        }

        ivLogo.setImage(new Image("/fxml/icons/ico.png"));

        initComponentListeners();

        initComponentImage();

        makeNavigation();

        buildTable();
    }

    /**
     * Init Components Listeners
     */
    private void initComponentListeners() {
        btnCancel.setOnAction(event -> {
            hyperlink.fire();
        });

        btnSave.setOnAction(event -> {
            if (operation == Operation.CREATE) {

                test.setName(txtName.getText());
                test.setSubject(txtSubject.getText());
                test.setCreatedDate(new Timestamp(new Date().getTime()));
                test.setPlanificationDate(Timestamp.valueOf(dpPlanDate.getValue().atStartOfDay()));
                test.setTestType(catalogueService.findByCode("01-01"));
                test.setTestVal(new BigDecimal(txtVal.getText()));

                try {
                    testService.save(test);

                    AlertHelper.showAlert(getStackPane(), "Guardado", AlertHelper.AlertType.INFORMATION);
                } catch (DAOException e) {
                    AlertHelper.showAlert(getStackPane(), e.getMessage(), AlertHelper.AlertType.ERROR);
                }

                int index = 0;

                for (Question question : olQuestion) {

                    question.setTest(test);

                    try {

                        questionService.saveQuestion(question);

                    } catch (DAOException e) {
                        e.printStackTrace();
                    }

                    AnswersOfQuestion answersOfQuestion = answersOfQuestionList.get(index);

                    answersOfQuestion.getOlAnwser().forEach(answer -> {
                        try {

                            answer.setQuestion(question);

                            answerService.saveAnswer(answer);

                        } catch (DAOException e) {
                            e.printStackTrace();
                        }
                    });

                    index++;
                }

                hyperlink.fire();
            } else if (operation == Operation.EDIT) {
            }
        });

        btnUpload.setOnAction(event -> {

            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(Paths.get(System.getProperty("user.home")).toFile());
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Images files", "*.png", "*.PNG", "*.jpg", "*.JPG"));

            File file = fileChooser.showOpenDialog(primaryStage);

            if (file != null) {
                Image image = new Image("file:///".concat(file.getAbsolutePath()));

                img = ImageUtil.retrieveArrayImage(image);

                ivLogo.setImage(image);

                ivLogo.setFitWidth(64.0);
                ivLogo.setFitHeight(64.0);
            }

        });

        btnAddQuestion.setOnAction(event -> {
            Question question = new Question();

            question.setBody(txtQuestionBody.getText());
            question.setQuestionVal(new BigDecimal(txtQuestionVal.getText()));

            olQuestion.add(question);

            AnswersOfQuestion answersOfQuestion = new AnswersOfQuestion();

            answersOfQuestion.setQuestion(question);
            answersOfQuestion.setOlAnwser(FXCollections.observableArrayList());

            int answerNumber = Integer.valueOf(txtQuestionNumber.getText());

            char initial = 'A';

            for (int i = 0; i < answerNumber; i++) {
                Answer answer = new Answer();

                if (i == 0) {
                    answer.setCorrect(true);
                } else {
                    answer.setCorrect(false);
                }

                answer.setBody(String.valueOf(initial));
                answersOfQuestion.getOlAnwser().add(answer);

                initial++;
            }

            answersOfQuestionList.add(answersOfQuestion);
        });
    }

    /**
     * Init Component Image
     */
    private void initComponentImage() {
        btnSave.setGraphic(IconApply.createIcon(FontAwesomeIcon.SAVE, "12"));
        btnCancel.setGraphic(IconApply.createIcon(FontAwesomeIcon.BAN, "12"));
        btnPreview.setGraphic(IconApply.createIcon(FontAwesomeIcon.FILE_PDF_ALT, "12"));
        btnUpload.setGraphic(IconApply.createIcon(FontAwesomeIcon.UPLOAD));
    }

    /**
     * Build Table
     */
    private void buildTable() {

        Callback<TableColumn<Question, String>, TableCell<Question, String>> integerCallBack =
                p -> new TextFieldCellAutoCommit<>(TextFieldCharSet.INTEGER_INPUT, "-fx-alignment: center");

        Callback<TableColumn<Question, String>, TableCell<Question, String>> stringCallBack =
                p -> new TextFieldCellAutoCommit<>(TextFieldCharSet.NONE, "-fx-alignment: center");

        Callback<TableColumn<Answer, String>, TableCell<Answer, String>> integerCallBackAnswer =
                p -> new TextFieldCellAutoCommit<>(TextFieldCharSet.INTEGER_INPUT, "-fx-alignment: center");

        Callback<TableColumn<Answer, String>, TableCell<Answer, String>> stringCallBackAnswer =
                p -> new TextFieldCellAutoCommit<>(TextFieldCharSet.NONE, "-fx-alignment: center");


        Callback<TableColumn<Answer, Boolean>, TableCell<Answer, Boolean>> booleanCallBack =
                p -> new CheckBoxCell<>(Pos.CENTER);

        questionBodyColumn.setCellFactory(stringCallBack);
        questionBodyColumn.setCellValueFactory(cellValue -> new SimpleStringProperty(cellValue.getValue().getBody()));
        questionBodyColumn.setOnEditCommit(t -> {
            if (t.getNewValue() != null) {
                Question question = t.getTableView().getItems().get(t.getTablePosition().getRow());

                question.setBody(t.getNewValue());

                tblQuestion.refresh();
            }
        });

        valQuestionColumn.setCellFactory(integerCallBack);
        valQuestionColumn.setCellValueFactory(cellValue -> new SimpleStringProperty(String.valueOf(cellValue.getValue().getQuestionVal().intValue())));
        valQuestionColumn.setOnEditCommit(t -> {
            if (t.getNewValue() != null) {
                Question question = t.getTableView().getItems().get(t.getTablePosition().getRow());

                question.setQuestionVal(new BigDecimal(t.getNewValue()));

                tblQuestion.refresh();
            }
        });

        actionQuestionColumn.setCellFactory(column -> {
            TableCell<Question, String> cell = new TableCell<Question, String>() {
                final JFXButton btnAdditionalInfo = new JFXButton();

                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    Question question = (Question) getTableRow().getItem();

                    btnAdditionalInfo.setPrefWidth(40);
                    btnAdditionalInfo.setPrefHeight(26);
                    btnAdditionalInfo.setButtonType(JFXButton.ButtonType.RAISED);
                    btnAdditionalInfo.setGraphic(IconApply.createIcon(FontAwesomeIcon.EYE));
                    btnAdditionalInfo.getStyleClass().add("warning");

                    if (question != null && !empty) {

                        setGraphic(btnAdditionalInfo);

                        btnAdditionalInfo.setOnAction(event -> {
                            answersOfQuestionList.forEach(answersOfQuestion -> {
                                tblAnswer.setItems(answersOfQuestion.getOlAnwser());

                                tblAnswer.refresh();

                                tab.getSelectionModel().select(1);
                            });
                        });
                    }
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };

            cell.setStyle("-fx-alignment: center-left;");

            return cell;
        });

        questionBodyColumn.setCellValueFactory(cellValue -> new SimpleStringProperty(cellValue.getValue().getBody()));

        bodyAnswerColumn.setCellFactory(stringCallBackAnswer);
        bodyAnswerColumn.setCellValueFactory(cellValue -> new SimpleStringProperty(cellValue.getValue().getBody()));
        bodyAnswerColumn.setOnEditCommit(t -> {
            if (t.getNewValue() != null) {
                Answer question = t.getTableView().getItems().get(t.getTablePosition().getRow());

                question.setBody(t.getNewValue());

                tblAnswer.refresh();
            }
        });
        checkAnswerColumn.setCellFactory(booleanCallBack);
        checkAnswerColumn.setCellValueFactory(cellValue -> new SimpleBooleanProperty(cellValue.getValue().getCorrect()));
        checkAnswerColumn.setOnEditCommit(t -> {
            if (t.getNewValue() != null) {
                Answer answer = t.getTableView().getItems().get(t.getTablePosition().getRow());

                t.getTableView().getItems().forEach(a -> {
                    a.setCorrect(false);
                });

                answer.setCorrect(t.getNewValue());

                tblAnswer.refresh();
            }
        });

        actionAnsweColumn.setVisible(false);
    }

    /**
     * Make Navigation
     */
    private void makeNavigation() {
        if (navigation != null) {

            hyperlink = BreadCrumbUtil.retrieveLastHyperLink(navigation);

            borderPane.setTop(navigation.getParent());

            BorderPane.setMargin(navigation.getParent(), new Insets(0, 0, 0, 0));
            BorderPane.setAlignment(navigation.getParent(), Pos.CENTER);

            BreadCrumbUtil.addHyperLink(navigation, "Crear", true, event -> {

                BreadCrumbUtil.removeBeforeHyperLink(navigation, "Crear");

                ViewHelper.setNode(getPanel(), anchorPane);
            });
        }
    }

    public HBox getNavigation() {
        return navigation;
    }

    public void setNavigation(HBox navigation) {
        this.navigation = navigation;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    class AnswersOfQuestion {

        private Question question;
        private ObservableList<Answer> olAnwser;

        public Question getQuestion() {
            return question;
        }

        public void setQuestion(Question question) {
            this.question = question;
        }

        public ObservableList<Answer> getOlAnwser() {
            return olAnwser;
        }

        public void setOlAnwser(ObservableList<Answer> olAnwser) {
            this.olAnwser = olAnwser;
        }
    }
}
