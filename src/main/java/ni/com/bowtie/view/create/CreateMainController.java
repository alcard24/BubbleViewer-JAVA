package ni.com.bowtie.view.create;

import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import ni.com.bowtie.businessLogic.service.CatalogueService;
import ni.com.bowtie.businessLogic.service.TestService;
import ni.com.bowtie.businessLogic.serviceImpl.CatalogueServiceImpl;
import ni.com.bowtie.businessLogic.serviceImpl.TestServiceImpl;
import ni.com.bowtie.model.entity.Catalogue;
import ni.com.bowtie.model.entity.Test;
import ni.com.bowtie.view.util.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ResourceBundle;


/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * <p>
 * 9/4/2018 6:59 PM
 */
public class CreateMainController extends BaseController implements Initializable {

    private Logger logger = Logger.getLogger(CreateMainController.class);

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private BorderPane borderPane;
    @FXML
    private HBox pnlNavigation;
    @FXML
    private JFXCheckBox ckbTypeTest;
    @FXML
    private JFXCheckBox ckbTestName;
    @FXML
    private JFXCheckBox ckbDate;
    @FXML
    private JFXButton btnSearch;
    @FXML
    private ComboBox<Catalogue> cmbTestType;
    @FXML
    private TextField txtName;
    @FXML
    private DatePicker dpFromDate;
    @FXML
    private DatePicker dpToDate;
    @FXML
    private JFXRadioButton rdbTestType;
    @FXML
    private JFXRadioButton rdbName;
    @FXML
    private JFXRadioButton rdbDate;
    @FXML
    private TableView<Test> tblItems;
    @FXML
    private TableColumn<Test, String> nameColumn;
    @FXML
    private TableColumn<Test, String> subjectColumn;
    @FXML
    private TableColumn<Test, String> testTypeColumn;
    @FXML
    private TableColumn<Test, String> createdDateColumn;
    @FXML
    private TableColumn<Test, String> actionColumn;
    @FXML
    private Pagination pagination;
    @FXML
    private JFXButton btnCreateExam;

    // Object List
    private ObservableList<Catalogue> olTestType = FXCollections.observableArrayList();
    private ObservableList<Test> olTest = FXCollections.observableArrayList();

    // Service
    private TestService testService = new TestServiceImpl();
    private CatalogueService catalogueService = new CatalogueServiceImpl();

    // Object
    private Catalogue testType;

    private String name;

    private Timestamp fromDate;

    private Timestamp toDate;

    private Integer total;

    private OrderByMain orderByMain = OrderByMain.NONE;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        this.resources = resources;

        initComponents();

        initComponentsValues();

        initComponentsListeners();

        makeNavigation();
    }


    /**
     * Init Components
     */
    private void initComponents() {
        rdbName.setSelected(true);

        initComponentImage();

        buildTable();


    }

    /**
     * Init Components Values
     */
    private void initComponentsValues() {
        olTestType = FXCollections.observableArrayList(catalogueService.findAllByMasterCode("01"));

        cmbTestType.setItems(olTestType);

        cmbTestType.getSelectionModel().selectFirst();

        initTestRecords(true);
    }

    /**
     * Init Components Listeners
     */
    private void initComponentsListeners() {

        btnCreateExam.setOnAction(event -> {

//            ViewHelper.showPartial(
//                    getClass().getResource("/fxml/dashboard/create/Create.fxml"),
//                    getPanel(),
//                    getPanel(),
//                    getPrimaryStage(),
//                    getStackPane(),
//                    getMaskerPane(),
//                    new ChangeView() {
//                        @Override
//                        public void afterDefaultSet(BaseController baseController) {
//                        }
//
//                        @Override
//                        public void beforeDefaultSet(BaseController baseController) {
//
//                            CreateController createController = (CreateController) baseController;
//
//                            createController.setNavigation(pnlNavigation);
//                            createController.setTest(new Test());
//                            createController.setOperation(Operation.CREATE);
//                            createController.initComponents();
//                        }
//                    },
//                    this.getUser()
//            );

            this.callTestCreator(null);

        });

        pagination.currentPageIndexProperty().addListener(((observable, oldValue, newValue) -> {
            initTestRecords(false);
        }));
    }


    /**
     * Set Panel Header
     */
    private void makeNavigation() {

        BreadCrumbUtil.addHyperLink(pnlNavigation, "Examenes", false, event -> {

            ViewHelper.setNode(getPanel(), anchorPane);

            BreadCrumbUtil.removeBeforeHyperLink(pnlNavigation, "Examenes");

            borderPane.setTop(pnlNavigation.getParent());

            BorderPane.setMargin(pnlNavigation.getParent(), new Insets(0, 0, 0, 0));
            BorderPane.setAlignment(pnlNavigation.getParent(), Pos.CENTER);

            btnSearch.fire();
        });


    }

    /**
     * Init Component Image
     */
    private void initComponentImage() {
        btnSearch.setGraphic(IconApply.createIcon(FontAwesomeIcon.SEARCH));

        btnSearch.setTooltip(createTooltip("Buscar"));

//        btnCreateExam.setGraphic(IconApply.createIcon(FontAwesomeIcon.PLUS, "16"));
    }

    /**
     * Init Test Records
     *
     * @param init, Init
     */
    private void initTestRecords(boolean init) {

        int startRow = 0;

        if (rdbName.isSelected()) {

            orderByMain = OrderByMain.NAME;

        } else if (rdbTestType.isSelected()) {

            orderByMain = OrderByMain.TEST_TYPE;

        } else if (rdbDate.isSelected()) {

            orderByMain = OrderByMain.DATE;

        }

        if (init) {

            setSearch();

            startRow = pagination.getCurrentPageIndex() * maxRows;

            total = testService.findTotalByFilter(testType, name, toDate, fromDate);
        }

        olTest = FXCollections.observableArrayList(testService.findByFilterPaged(testType, name, toDate, fromDate, orderByMain, startRow, maxRows));

        setPagination(pagination, total);

        tblItems.setItems(olTest);

        tblItems.refresh();
    }

    /**
     * Build Table
     */
    private void buildTable() {
        nameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        nameColumn.setCellFactory(t -> retrieveSimpleStringCell(""));
        subjectColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSubject()));
        subjectColumn.setCellFactory(t -> retrieveSimpleStringCell(""));
        testTypeColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTestType().getName()));
        testTypeColumn.setCellFactory(t -> retrieveSimpleStringCell(""));
        createdDateColumn.setCellValueFactory(cellData -> new SimpleStringProperty(DateUtil.diffForHumans(cellData.getValue().getCreatedDate())));
        createdDateColumn.setCellFactory(t -> retrieveSimpleStringCell(""));
        actionColumn.setCellFactory(column -> {
            TableCell<Test, String> cell = new TableCell<Test, String>() {

                final JFXButton btnPrintTest = createOptionButtonFlat(FontAwesomeIcon.PRINT, "Imprimir");

                final Button btnEditTest = createOptionButtonFlat(FontAwesomeIcon.PENCIL, "Editar");

                final JFXButton btnLock = createOptionButtonFlat(FontAwesomeIcon.LOCK, "Cerrar");

                final JFXButton btnPreview = createOptionButtonFlat(FontAwesomeIcon.EYE, "Vista previa");

                final JFXButton btnDelete = createOptionButtonFlat(FontAwesomeIcon.BAN, "Eliminar");


                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);

                    HBox hbox = new HBox(5);

                    Test test = (Test) getTableRow().getItem();

                    hbox.getChildren().addAll(btnPrintTest, btnPreview, btnEditTest, btnLock, btnDelete);

                    if (test != null && !empty) {

                        setGraphic(hbox);

                        btnEditTest.setOnAction(event -> {

                            callTestCreator(test);

                        });

                        btnPreview.setOnAction(event -> {
                            try {
                                PdfProcess.generatePreview(getClass(), getPrimaryStage(), test, getMaskerPane());
                            } catch (IOException e) {
                                AlertHelper.showAlert(getStackPane(), "", AlertHelper.AlertType.ERROR);
                            }
                        });
                    }
                }

                private String getString() {
                    return getItem() == null ? "" : getItem();
                }
            };

            cell.setStyle("-fx-alignment: center-left;");

            return cell;
        });
    }

    private void callTestCreator(Test test){
        ViewHelper.showPartial(
                getClass().getResource("/fxml/dashboard/create/CreateTest.fxml"),
                getPanel(),
                getPanel(),
                getPrimaryStage(),
                getStackPane(),
                getMaskerPane(),
                new ChangeView() {
                    @Override
                    public void afterDefaultSet(BaseController baseController) {
                    }

                    @Override
                    public void beforeDefaultSet(BaseController baseController) {

                        CreateTestController createController = (CreateTestController) baseController;

                        createController.setNavigation(pnlNavigation);

                        createController.setTest(test);

                        createController.initComponents();

                    }
                },
                this.getUser()
        );
    }

    /**
     * Retrieve Simple String Cell
     *
     * @return TableCell<StockReceipts>
     **/
    private TableCell<Test, String> retrieveSimpleStringCell(String style) {

        TableCell<Test, String> cell = new TableCell<Test, String>() {
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : this.getString());
                setGraphic(null);
            }

            private String getString() {
                return getItem() == null ? "" : getItem();
            }
        };

        cell.setStyle(style);

        return cell;
    }

    /**
     * Set Search
     */
    private void setSearch() {
        if (ckbTypeTest.isSelected()) {
            testType = cmbTestType.getSelectionModel().getSelectedItem();
        } else {
            testType = null;
        }
        if (ckbDate.isSelected()) {
            if (dpFromDate.getValue() != null && dpToDate.getValue() != null) {
                fromDate = Timestamp.valueOf(dpFromDate.getValue().atStartOfDay());
                toDate = Timestamp.valueOf(dpToDate.getValue().atStartOfDay());
            }
        } else {
            fromDate = null;
            toDate = null;
        }
        if (ckbTestName.isSelected()) {
            name = txtName.getText();
        } else {
            name = null;
        }
    }


    @FXML
    private void btnSearch_onAction(ActionEvent actionEvent) {
        initTestRecords(true);
    }
}
