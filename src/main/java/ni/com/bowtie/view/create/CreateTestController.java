package ni.com.bowtie.view.create;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.HTMLEditor;
import javafx.util.StringConverter;
import ni.com.bowtie.businessLogic.service.AnswerService;
import ni.com.bowtie.businessLogic.service.CatalogueService;
import ni.com.bowtie.businessLogic.service.QuestionService;
import ni.com.bowtie.businessLogic.service.TestService;
import ni.com.bowtie.businessLogic.serviceImpl.AnswerServiceImpl;
import ni.com.bowtie.businessLogic.serviceImpl.CatalogueServiceImpl;
import ni.com.bowtie.businessLogic.serviceImpl.QuestionServiceImpl;
import ni.com.bowtie.businessLogic.serviceImpl.TestServiceImpl;
import ni.com.bowtie.model.entity.Catalogue;
import ni.com.bowtie.model.entity.Test;
import ni.com.bowtie.view.util.*;
import org.apache.log4j.Logger;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;


/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * <p>
 * 9/4/2018 6:59 PM
 */
public class CreateTestController extends BaseController implements Initializable {

    Logger logger = Logger.getLogger(CreateTestController.class);

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private BorderPane borderPane;
    @FXML
    private Accordion accordion;
    @FXML
    private TitledPane tpConfiguration;
    @FXML
    private TextField txtTitle;
    @FXML
    private TextField txtValue;
    @FXML
    private ChoiceBox<Catalogue> cbTopic;
    @FXML
    private JFXButton btnAddTopic;
    @FXML
    private DatePicker dpDate;
    @FXML
    private ImageView ivImage;
    @FXML
    private JFXButton btnChangeImage;
    @FXML
    private JFXButton btnDefaultImage;
    @FXML
    private JFXCheckBox ckbDescription;
    @FXML
    private HTMLEditor heDescription;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnPreview;

    private HBox navigation;
    private Hyperlink hyperlink;


    // Object List
    private ObservableList<Catalogue> olTestType = FXCollections.observableArrayList();
    private ObservableList<Test> olTest = FXCollections.observableArrayList();

    // Object
    private Test test = null;

    // Service
    private TestService testService = new TestServiceImpl();
    private CatalogueService catalogueService = new CatalogueServiceImpl();
    private QuestionService questionService = new QuestionServiceImpl();
    private AnswerService answerService = new AnswerServiceImpl();


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        this.resources = resources;

    }


    /**
     * Init Components
     */
    public void initComponents() {

        initComponentsValues();

        initComponentsListeners();

        makeNavigation();

        initComponentImage();

        buildTable();

        initToolTips();
    }

    /**
     * Init Components Values
     */
    private void initComponentsValues() {

        olTestType = FXCollections.observableArrayList(catalogueService.findAllByMasterCode("01"));

        olTestType.add(0, null);

        cbTopic.setItems(olTestType);

        cbTopic.setConverter(new StringConverter<Catalogue>() {
            @Override
            public String toString(Catalogue object) {
                if (object == null) {
                    return "Ninguno";
                } else {
                    return object.toString();
                }
            }

            @Override
            public Catalogue fromString(String string) {
                return null;
            }
        });

        cbTopic.getSelectionModel().selectFirst();

        txtValue.setText("0");

        accordion.setExpandedPane(tpConfiguration);

        Pattern pattern = Pattern.compile(TextFieldCharSet.DOUBLE_INPUT.getText());

        TextFormatter formatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
            return pattern.matcher(change.getControlNewText()).matches() ? change : null;
        });

        txtValue.setTextFormatter(formatter);

        if (test != null) {

            txtTitle.setText(test.getName());
            txtValue.setText(test.getMinimalApovedScore().toString());

        } else {

            ckbDescription.setSelected(true);

            heDescription.setHtmlText(catalogueService.findByCode("02-01").getName());

        }

        initTestRecords(true);
    }

    /**
     * Init Components Listeners
     */
    private void initComponentsListeners() {

        btnPreview.setOnAction(event -> {

            String clip = heDescription.getHtmlText();

            StringSelection stringSelection = new StringSelection(clip);
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(stringSelection, null);
        });
    }


    /**
     * Set Panel Header
     */
    private void makeNavigation() {

        if (navigation != null) {

            hyperlink = BreadCrumbUtil.retrieveLastHyperLink(navigation);

            borderPane.setTop(navigation.getParent());

            BorderPane.setMargin(navigation.getParent(), new Insets(0, 0, 0, 0));
            BorderPane.setAlignment(navigation.getParent(), Pos.CENTER);

            BreadCrumbUtil.addHyperLink(navigation, "Crear examen", true, event -> {

                BreadCrumbUtil.removeBeforeHyperLink(navigation, "Crear examen");

                ViewHelper.setNode(getPanel(), anchorPane);
            });
        }


    }

    /**
     * Init Component Image
     */
    private void initComponentImage() {

        ivImage.setImage(new Image("/fxml/icons/ico.png"));

        btnAddTopic.setGraphic(IconApply.createIcon(FontAwesomeIcon.PLUS_CIRCLE));

//        btnCancel.setGraphic(IconApply.createIcon(FontAwesomeIcon.STOP, "16"));
//        btnSave.setGraphic(IconApply.createIcon(FontAwesomeIcon.SAVE, "16"));
//        btnPreview.setGraphic(IconApply.createIcon(FontAwesomeIcon.PRINT, "16"));

    }

    /**
     * Init Tool Tips
     */
    private void initToolTips(){

        btnAddTopic.setTooltip(createTooltip("Crea un nuevo tema"));

    }

    /**
     * Init Test Records
     *
     * @param init, Init
     */
    private void initTestRecords(boolean init) {

    }

    /**
     * Build Table
     */
    private void buildTable() {

    }


    @FXML
    private void btnSearch_onAction(ActionEvent actionEvent) {
        initTestRecords(true);
    }

    public HBox getNavigation() {
        return navigation;
    }

    public void setNavigation(HBox navigation) {
        this.navigation = navigation;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }
}
