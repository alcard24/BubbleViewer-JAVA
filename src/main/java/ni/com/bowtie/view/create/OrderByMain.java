package ni.com.bowtie.view.create;

/**
 *
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 *
 */
public enum OrderByMain {
    NONE,
    TEST_TYPE,
    NAME,
    DATE
}
