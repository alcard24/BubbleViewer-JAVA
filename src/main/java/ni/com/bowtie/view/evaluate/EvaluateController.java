package ni.com.bowtie.view.evaluate;

import com.jfoenix.controls.JFXButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import ni.com.bowtie.businessLogic.service.AnswerService;
import ni.com.bowtie.businessLogic.service.QuestionService;
import ni.com.bowtie.businessLogic.service.TestService;
import ni.com.bowtie.businessLogic.serviceImpl.AnswerServiceImpl;
import ni.com.bowtie.businessLogic.serviceImpl.QuestionServiceImpl;
import ni.com.bowtie.businessLogic.serviceImpl.TestServiceImpl;
import ni.com.bowtie.model.entity.Answer;
import ni.com.bowtie.model.entity.Question;
import ni.com.bowtie.model.entity.Test;
import ni.com.bowtie.view.util.*;
import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;
import java.util.ResourceBundle;

/**
 * <p>
 * Bow Tie Product 2017 - 2019
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 5/8/2019 10:59 AM
 */
public class EvaluateController extends BaseController implements Initializable {

    private Logger logger = Logger.getLogger(EvaluateController.class);
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private BorderPane borderPane;
    @FXML
    private HBox pnlNavigation;
    @FXML
    private JFXButton btnRevisar;
    @FXML
    private ChoiceBox<Test> cbTest;
    @FXML
    private ImageView ivImage;
    @FXML
    private JFXButton btnCargarImagen;

    private TestService testService = new TestServiceImpl();
    private QuestionService questionService = new QuestionServiceImpl();
    private AnswerService answerService = new AnswerServiceImpl();

    private String answerFile = null;

    private String pythonFilePart1 = "# USAGE\n" +
            "# python test_grader.py --image images/test_01.png\n" +
            "\n" +
            "# import the necessary packages\n" +
            "import cv2\n" +
            "from imutils.perspective import four_point_transform\n" +
            "from imutils import contours\n" +
            "import numpy as np\n" +
            "import argparse\n" +
            "import imutils\n" +
            "\n" +
            "# construct the argument parse and parse the arguments\n" +
            "ap = argparse.ArgumentParser()\n" +
            "ap.add_argument(\"-i\", \"--image\", required=True,\n" +
            "                help=\"path to the input image\")\n" +
            "args = vars(ap.parse_args())\n" +
            "\n" +
            "# define the answer key which maps the question number\n" +
            "# to the correct answer\n" +
            "ANSWER_KEY =  \\\n" +
            "    [";

    private String pythonFilePart2 = "]\n" +
            "\n" +
            "\n" +
            "# load the image, convert it to grayscale, blur it\n" +
            "# slightly, then find edges\n" +
            "\n" +
            "image = cv2.imread(args[\"image\"])\n" +
            "gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)\n" +
            "blurred = cv2.GaussianBlur(gray, (5, 5), 0)\n" +
            "# edged = cv2.Canny(blurred, 75, 200)\n" +
            "\n" +
            "edged = cv2.Canny(blurred, 5,5)\n" +
            "\n" +
            "\n" +
            "\n" +
            "# find contours in the edge map, then initialize\n" +
            "# the contour that corresponds to the document\n" +
            "cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,\n" +
            "                        cv2.CHAIN_APPROX_SIMPLE)\n" +
            "cnts = cnts[0] if imutils.is_cv2() else cnts[1]\n" +
            "docCnt = None\n" +
            "\n" +
            "# ensure that at least one contour was found\n" +
            "if len(cnts) > 0:\n" +
            "    # sort the contours according to their size in\n" +
            "    # descending order\n" +
            "    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)\n" +
            "\n" +
            "    # loop over the sorted contours\n" +
            "    for c in cnts:\n" +
            "        # approximate the contour\n" +
            "        peri = cv2.arcLength(c, True)\n" +
            "        approx = cv2.approxPolyDP(c, 0.02 * peri, True)\n" +
            "\n" +
            "        # if our approximated contour has four points,\n" +
            "        # then we can assume we have found the paper\n" +
            "        if len(approx) == 4:\n" +
            "            docCnt = approx\n" +
            "            break\n" +
            "\n" +
            "# apply a four point perspective transform to both the\n" +
            "# original image and grayscale image to obtain a top-down\n" +
            "# birds eye view of the paper\n" +
            "paper = four_point_transform(image, docCnt.reshape(4, 2))\n" +
            "warped = four_point_transform(gray, docCnt.reshape(4, 2))\n" +
            "\n" +
            "# apply Otsu's thresholding method to binarize the warped\n" +
            "# piece of paper\n" +
            "thresh = cv2.threshold(warped, 0, 50,\n" +
            "                       cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]\n" +
            "\n" +
            "# find contours in the thresholded image, then initialize\n" +
            "# the list of contours that correspond to questions\n" +
            "cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,\n" +
            "                        cv2.CHAIN_APPROX_SIMPLE)\n" +
            "cnts = cnts[0] if imutils.is_cv2() else cnts[1]\n" +
            "questionCnts = []\n" +
            "\n" +
            "# loop over the contours\n" +
            "\n" +
            "#revisando si el examen corresponde al mapeo\n" +
            "if(len(questionCnts) > len(cnts)) :\n" +
            "    print(\"Este examen no corresponde al que se va a revisar\")\n" +
            "\n" +
            "for c in cnts:\n" +
            "    # compute the bounding box of the contour, then use the\n" +
            "    # bounding box to derive the aspect ratio\n" +
            "    (x, y, w, h) = cv2.boundingRect(c)\n" +
            "    ar = w / float(h)\n" +
            "\n" +
            "    # in order to label the contour as a question, region\n" +
            "    # should be sufficiently wide, sufficiently tall, and\n" +
            "    # have an aspect ratio approximately equal to 1\n" +
            "    if w >= 20 and h >= 20 and ar >= 0.9 and ar <= 1.1:\n" +
            "        questionCnts.append(c)\n" +
            "\n" +
            "# sort the question contours top-to-bottom, then initialize\n" +
            "# the total number of correct answers\n" +
            "questionCnts = contours.sort_contours(questionCnts,\n" +
            "                                      method=\"top-to-bottom\")[0]\n" +
            "correct = 0\n" +
            "\n" +
            "\n" +
            "# each question has 5 possible answers, to loop over the\n" +
            "# question in batches of 5\n" +
            "for (q, i) in enumerate(np.arange(0, len(questionCnts), 5)):\n" +
            "    # sort the contours for the current question from\n" +
            "    # left to right, then initialize the index of the\n" +
            "    # bubbled answer\n" +
            "    cnts = contours.sort_contours(questionCnts[i:i + 5])[0]\n" +
            "   # bubbled = None\n" +
            "    str = \"\"\n" +
            "    # loop over the sorted contours\n" +
            "    index = []\n" +
            "    for (j, c) in enumerate(cnts):\n" +
            "        # construct a mask that reveals only the current\n" +
            "        # \"bubble\" for the question\n" +
            "        mask = np.zeros(thresh.shape, dtype=\"uint8\")\n" +
            "        cv2.drawContours(mask, [c], -1, 255, -1)\n" +
            "\n" +
            "        # apply the mask to the thresholded image, then\n" +
            "        # count the number of non-zero pixels in the\n" +
            "        # bubble area\n" +
            "        mask = cv2.bitwise_and(thresh, thresh, mask=mask)\n" +
            "        total = cv2.countNonZero(mask)\n" +
            "\n" +
            "        # if the current total has a larger number of total\n" +
            "        # non-zero pixels, then we are examining the currently\n" +
            "        # bubbled-in answer\n" +
            "        #bubbled = (700,-1)\n" +
            "\n" +
            "        if total > 300:\n" +
            "            str = str + \"1\"\n" +
            "            index.append(j)\n" +
            "        else:\n" +
            "            str = str + \"0\"\n" +
            "\n" +
            "\n" +
            "\n" +
            "    # initialize the contour color and the index of the\n" +
            "    # *correct* answer\n" +
            "    color = (0, 0, 255)\n" +
            "\n" +
            "    #str contains the answer in bits 010101\n" +
            "    print(str)\n" +
            "\n" +
            "    b = (int(str,2)) #Convert to number\n" +
            "    a = (int(ANSWER_KEY[q], 2)) #Convert to number\n" +
            "    k =  (a - b) == 0  # if the diff is zero is correct\n" +
            "\n" +
            "    # check to see if the bubbled answer is correct\n" +
            "    if k:\n" +
            "        color = (0, 255, 0) #correct color for resalt\n" +
            "        correct += 1\n" +
            "    # draw the outline of the correct answer on the test\n" +
            "\n" +
            "\n" +
            "    #Draw all of answer from the user\n" +
            "    if len(index):\n" +
            "        for p in index:\n" +
            "            cv2.drawContours(paper, [cnts[p]], -1, color, 3)\n" +
            "\n" +
            "# grab the test taker\n" +
            "score = (correct / 5.0) * 100\n" +
            "print(\"[INFO] puntuación: {:.2f}%\".format(score))\n" +
            "cv2.putText(paper, \"{:.2f}%\".format(score), (10, 30),\n" +
            "            cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 0, 255), 2)\n" +
            "cv2.imshow(\"Original\", image)\n" +
            "cv2.imshow(\"Exam\", paper)\n" +
            "cv2.waitKey(0)\n";

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        initComponents();

        initComponentsValues();

        initComponentsListeners();

        makeNavigation();
    }

    private void makeNavigation() {

        BreadCrumbUtil.addHyperLink(pnlNavigation, "Revision", false, event -> {

            ViewHelper.setNode(getPanel(), anchorPane);

            BreadCrumbUtil.removeBeforeHyperLink(pnlNavigation, "Revision");

            borderPane.setTop(pnlNavigation.getParent());

            BorderPane.setMargin(pnlNavigation.getParent(), new Insets(0, 0, 0, 0));
            BorderPane.setAlignment(pnlNavigation.getParent(), Pos.CENTER);

        });
    }

    private void initComponentsListeners() {

        btnRevisar.setOnAction(event ->
        {
            try {

                if (cbTest.getValue() != null && answerFile != null) {

                    StringBuilder pattern = new StringBuilder();


                    List<Question> lstQuestion = questionService.findAllQuestionByTest(cbTest.getValue());

                    for (Question question : lstQuestion) {

                        if (!pattern.toString().equalsIgnoreCase("")) {
                            pattern.append(",\n     ");
                        }
                        pattern.append("\"");
                        List<Answer> lstAnswer = answerService.findAllAnswerByQuestion(question);

                        for (Answer answer : lstAnswer) {
                            pattern.append(answer.getCorrect() ? "1" : "0");
                        }

                        pattern.append("\"");
                    }


                    String fileContent = pythonFilePart1 + pattern.toString() + pythonFilePart2;

                    File yourFile = new File("process.py");

                    yourFile.createNewFile();

                    BufferedWriter writer = new BufferedWriter(new FileWriter("process.py"));
                    writer.write(fileContent);

                    writer.close();

                    Runtime.getRuntime().exec("python.exe " + yourFile.getAbsolutePath() + "  --image=" + answerFile);

                } else {

                    AlertHelper.showAlert(stackPane, "Seleccione un test a evaluar y su hoja de respuestas", AlertHelper.AlertType.ERROR);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnCargarImagen.setOnAction(event -> {

            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialDirectory(Paths.get(System.getProperty("user.home")).toFile());
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Images files", "*.png", "*.PNG", "*.jpg", "*.JPG"));

            File file = fileChooser.showOpenDialog(primaryStage);

            if (file != null) {

                answerFile = file.getAbsolutePath();

                Image image = new Image("file:///".concat(file.getAbsolutePath()));

                byte[] img = ImageUtil.retrieveArrayImage(image);

                ivImage.setImage(image);

            }

        });
    }

    private void initComponentsValues() {
        ObservableList<Test> olTestType = FXCollections.observableArrayList(testService.findAll());

        olTestType.add(0, null);

        cbTest.setItems(olTestType);

        cbTest.setConverter(new StringConverter<Test>() {
            @Override
            public String toString(Test object) {
                if (object == null) {
                    return "Ninguno";
                } else {
                    return object.getName() + " - " + object.getSubject();
                }
            }

            @Override
            public Test fromString(String string) {
                return null;
            }
        });

        cbTest.getSelectionModel().selectFirst();

    }

    /**
     * Init Components
     */
    public void initComponents() {
        try {

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

}
