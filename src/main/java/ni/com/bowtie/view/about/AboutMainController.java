package ni.com.bowtie.view.about;

import javafx.fxml.Initializable;
import ni.com.bowtie.view.util.BaseController;

import java.net.URL;
import java.util.ResourceBundle;

/**
 *
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 7:37 PM
 *
 * */
public class AboutMainController extends BaseController implements Initializable {

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resources = resources;
    }
}
