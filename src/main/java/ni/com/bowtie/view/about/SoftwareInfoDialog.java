package ni.com.bowtie.view.about;

import javafx.fxml.Initializable;
import ni.com.bowtie.view.util.BaseController;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * <p>
 * Bow Tie Product 2017 - 2019
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 2/5/2019 3:01 PM
 */
public class SoftwareInfoDialog extends BaseController implements Initializable {


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
