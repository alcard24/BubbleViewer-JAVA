package ni.com.bowtie.view;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import ni.com.bowtie.businessLogic.service.UserService;
import ni.com.bowtie.businessLogic.serviceImpl.UserServiceImpl;
import ni.com.bowtie.model.entity.User;
import ni.com.bowtie.view.util.BaseController;
import ni.com.bowtie.view.util.ViewHelper;
import org.apache.log4j.Logger;
import org.controlsfx.control.MaskerPane;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/17/2018 2:12 AM
 */
public class ApplicationController extends BaseController implements Initializable {

    private Logger logger = Logger.getLogger(ApplicationController.class);

    @FXML
    private AnchorPane panel;
    @FXML
    private StackPane stackPn;
    @FXML
    private MaskerPane maskerPane;

    private UserService userService = new UserServiceImpl();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resources = resources;

        setStackPane(stackPn);
    }

    /**
     * Init Components
     */
    public void initComponents() {
        try {

            // First Attempt To Login...

            Preferences preferences = Preferences.userRoot();

            String user = preferences.get("user", null);
            String password = preferences.get("password", null);

            User loggedUser = null;

            if (user != null && password != null) {
                loggedUser = userService.login(user, password, false);
            }

            if (loggedUser != null) {

                ViewHelper.showDashboard(getClass(), panel, primaryStage, stackPane, maskerPane, loggedUser);
            } else {
                ViewHelper.showLogin(getClass(), panel, primaryStage, stackPane, maskerPane);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

}
