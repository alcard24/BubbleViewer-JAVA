package ni.com.bowtie.view;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
public class StartApp extends Application {

    @Override
    public void init() {
    }

    /***
     * Start of GUI Application
     * @param primaryStage, Primary Stage
     * @throws Exception, exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {

        Image image = new Image("/fxml/icons/ico.png");

        FXMLLoader fxmlLoader = new FXMLLoader();

        fxmlLoader.setLocation(getClass().getResource("/fxml/Application.fxml"));

        Parent root = fxmlLoader.load();
        ApplicationController applicationController = fxmlLoader.getController();

        Scene scene = new Scene(root);

        scene.getStylesheets().add("/css/style.css");

        primaryStage.setTitle("Gecko Express");
        primaryStage.setMaximized(true);
        primaryStage.getIcons().add(image);
        primaryStage.setScene(scene);
        primaryStage.show();

        primaryStage.setOnCloseRequest(event -> {

            JFXDialogLayout jfxDialogLayout = new JFXDialogLayout();

            jfxDialogLayout.setBody(new Label("¿Está seguro que desea salir de la aplicación?"));

            JFXDialog jfxDialog = new JFXDialog(applicationController.getStackPane(), jfxDialogLayout, JFXDialog.DialogTransition.CENTER);
            jfxDialog.setOverlayClose(false);

            JFXButton okButton = new JFXButton("Sí");
            JFXButton cancelButton = new JFXButton("No");

            okButton.getStyleClass().add("info");
            cancelButton.getStyleClass().add("info");

            okButton.setOnAction(eventOk -> {
                jfxDialog.close();
                primaryStage.close();
                System.exit(0);
            });

            cancelButton.setButtonType(JFXButton.ButtonType.FLAT);
            cancelButton.setTextFill(Color.valueOf("#26B99A"));
            cancelButton.setOnAction(eventCancel -> {
                jfxDialog.close();
                event.consume();
            });

            jfxDialogLayout.setActions(okButton, cancelButton);
            jfxDialog.show();

            event.consume();

        });

        applicationController.setPrimaryStage(primaryStage);
        applicationController.initComponents();
    }

    @Override
    public void stop() {
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
