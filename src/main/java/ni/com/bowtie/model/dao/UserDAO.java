package ni.com.bowtie.model.dao;

import ni.com.bowtie.model.entity.User;
import ni.com.bowtie.model.util.BaseGenericDAO;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/16/2018 10:48 PM
 */
public interface UserDAO extends BaseGenericDAO<User, Integer> {
}
