package ni.com.bowtie.model.dao;

import ni.com.bowtie.model.entity.Catalogue;
import ni.com.bowtie.model.util.BaseGenericDAO;

/**
 * <p>
 * Bow Tie Product 2017 - 2019
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 2/2/2019 10:51 PM
 */
public interface CatalogueDAO extends BaseGenericDAO<Catalogue, Integer> {
}
