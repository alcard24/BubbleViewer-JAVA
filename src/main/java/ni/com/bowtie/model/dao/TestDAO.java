package ni.com.bowtie.model.dao;

import ni.com.bowtie.model.entity.Test;
import ni.com.bowtie.model.util.BaseGenericDAO;

/**
 *
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 *
 */
public interface TestDAO extends BaseGenericDAO <Test, Integer>{
}
