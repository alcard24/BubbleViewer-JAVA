package ni.com.bowtie.model.dao;

import ni.com.bowtie.model.entity.Question;
import ni.com.bowtie.model.util.BaseGenericDAO;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/18/2018 12:06 AM
 */
public interface QuestionDAO extends BaseGenericDAO<Question, Integer> {
}
