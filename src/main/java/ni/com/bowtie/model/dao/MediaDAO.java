package ni.com.bowtie.model.dao;

import ni.com.bowtie.model.entity.Media;
import ni.com.bowtie.model.util.BaseGenericDAO;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Fender Josué Mora Calero, fenmora@outlook.es
 * @version 1.0
 * </p>
 * 19-Oct-18 9:16 PM
 */
public interface MediaDAO extends BaseGenericDAO<Media,Integer> {
}
