package ni.com.bowtie.model.util;


/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
public class DAOException extends Exception {


    private static final long serialVersionUID = 5394136727929338065L;


    /**
     * Constructor of DAOException with a message
     *
     * @param msg, Message
     */
    public DAOException(String msg) {
        super(msg);
    }

    /**
     * Build a exception with a cause
     *
     * @param cause Cause
     */
    public DAOException(Throwable cause) {
        super(cause);
    }

    /**
     * Build a exception with a cause and message
     *
     * @param msg,   Message
     * @param cause, Cause
     */
    public DAOException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
