package ni.com.bowtie.model.util;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;

import javax.persistence.EntityNotFoundException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
public interface BaseGenericDAO<T, ID extends Serializable> {

    /**
     * Find by Entity Identification
     *
     * @param id, Identification
     * @return T
     * @throws EntityNotFoundException, Exception
     */
    T find(ID id) throws EntityNotFoundException;

    /**
     * Find By Entity Identifications Collection
     *
     * @param ids, Entity's Id Collection
     * @param <T>, Entity Type
     * @return T, Entities
     */
    <T> T[] find(Serializable... ids);

    /**
     * Save Entity
     *
     * @param entity, Entity
     * @return T, Entity Saved
     * @throws DAOException, Exception
     */
    T save(Object entity) throws DAOException;

    /**
     * Save Collection
     *
     * @param entities, Entities
     * @throws DAOException, Exception
     */
    void save(Object... entities) throws DAOException;

    /**
     * Save Upper
     *
     * @param entity, Entity
     * @return T, Entity Saved
     * @throws DAOException, Exception
     */
    T saveUpper(Object entity) throws DAOException;

    /**
     * Save Upper Collection
     *
     * @param entities, Entities
     * @throws DAOException, Exception
     */
    void saveUpper(Object... entities) throws DAOException;

    /**
     * Update Upper
     *
     * @param entity, Entity
     * @throws DAOException, Exception
     */
    void updateUpper(Object entity) throws DAOException;

    /**
     * Update
     *
     * @param entity, Entity
     * @throws DAOException, Exception
     */
    void update(Object entity) throws DAOException;

    /**
     * Remove
     *
     * @param entity, Entity
     * @return boolean
     */
    boolean remove(Object entity);

    /**
     * Remove
     *
     * @param entities, Entities
     */
    void remove(Object... entities);

    /**
     * Remove By Id
     *
     * @param id, Entity Identification
     * @return boolean
     */
    boolean removeId(Serializable id);

    /**
     * Remove All By Id
     *
     * @param ids, Entities's id
     */
    void removeId(Serializable... ids);

    /**
     * Find All
     *
     * @return List<T>, Collection
     */
    List<T> findAll();

    /**
     * Is Attached
     *
     * @param entity, Entity
     * @return boolean
     */
    boolean isAttached(Object entity);

    /**
     * Refresh
     *
     * @param entities, Entities
     */
    void refresh(Object... entities);

    /**
     * Flush
     */
    void flush();

    /**
     * Search
     *
     * @param search, ISearch object
     * @return List
     */
    List search(ISearch search);

    /**
     * Count By Search
     *
     * @param search, ISearch Object
     * @return int
     */
    int count(ISearch search);

    /**
     * Count
     *
     * @return int
     */
    int count();

    /**
     * Search and Count
     *
     * @param search, Search
     * @return SearchResult
     */
    SearchResult searchAndCount(ISearch search);

    /**
     * Lazy Search
     *
     * @param first,    First Row
     * @param pageSize, Page Size
     * @return SearchResult
     */
    SearchResult lazySearch(int first, int pageSize);

    /**
     * Lazy Search
     *
     * @param first,     First
     * @param pageSize,  Page Size
     * @param sortField, Sort Field
     * @param sortOrder, Sort Oder
     * @param filters,   Filters
     * @return SearchResult
     */
    SearchResult lazySearch(int first, int pageSize, String sortField,
                            String sortOrder, Map<String, Object> filters);

    /**
     * Search Unique
     *
     * @param search, Search
     * @return Object
     */
    Object searchUnique(ISearch search);

}
