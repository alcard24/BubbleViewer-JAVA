package ni.com.bowtie.model.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import javax.persistence.Entity;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    private static final ServiceRegistry serviceRegistry;


    private static String user_name = "postgres";
    private static String url = "jdbc:postgresql://127.0.0.1:5432/testchecker";
    private static String password = "1234";

    static {
        try {
            Configuration config = getConfiguration();

            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                    config.getProperties()).build();

            sessionFactory = config.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session openSession() {
        return sessionFactory.openSession();
    }

    /**
     * Get Configuration Session Manager
     *
     * @return Configuration
     */
    private static Configuration getConfiguration() {

        Configuration cfg = new Configuration();

        ClassPathScanningCandidateComponentProvider scanner =
                new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(Entity.class));

        scanner.findCandidateComponents("ni.com.bowtie.model.entity").forEach(
                entity -> {
                    try {
                        cfg.addAnnotatedClass(Class.forName(entity.getBeanClassName()));
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
        );


        cfg.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
        cfg.setProperty("hibernate.connection.url", url);
        cfg.setProperty("hibernate.connection.username", user_name);
        cfg.setProperty("hibernate.connection.password", password);
        cfg.setProperty("hibernate.show_sql", "false");
        cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        cfg.setProperty("hibernate.hbm2ddl.auto", "update");
        cfg.setProperty("hibernate.cache.provider_class", "org.hibernate.cache.NoCacheProvider");
        cfg.setProperty("hibernate.current_session_context_class", "thread");

        return cfg;
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
