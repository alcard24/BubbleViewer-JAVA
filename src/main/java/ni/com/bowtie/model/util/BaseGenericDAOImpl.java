package ni.com.bowtie.model.util;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.SearchResult;
import com.googlecode.genericdao.search.hibernate.HibernateMetadataUtil;
import com.googlecode.genericdao.search.hibernate.HibernateSearchProcessor;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import javax.persistence.EntityNotFoundException;
import java.io.Serializable;
import java.lang.reflect.*;
import java.util.*;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
public class BaseGenericDAOImpl<T, ID extends Serializable> implements BaseGenericDAO<T, ID> {

    private Logger logger = Logger.getLogger(BaseGenericDAOImpl.class);

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    private Class<T> entityClass;

    private HibernateSearchProcessor searchProcessor;

    private HibernateMetadataUtil metadataUtil;

    public BaseGenericDAOImpl() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];

        searchProcessor = HibernateSearchProcessor.getInstanceForSessionFactory(sessionFactory);
        metadataUtil = HibernateMetadataUtil.getInstanceForSessionFactory(sessionFactory);

    }

    @Override
    public T find(Serializable id) throws EntityNotFoundException {

        initTransaction();

        T entity = (T) this.getSession().get(entityClass, id);

        if (entity == null) {
            throw new EntityNotFoundException(id.toString());
        }

        endTransaction();

        return entity;
    }

    @Override
    public <T> T[] find(Serializable... ids) {
        initTransaction();

        Criteria c = getSession().createCriteria(entityClass);
        c.add(Restrictions.in("id", ids));
        Object[] retVal = (Object[]) Array.newInstance(entityClass, ids.length);

        for (Object entity : c.list()) {
            for (int i = 0; i < ids.length; i++) {
                retVal[i] = entity;
                break;
            }
        }


        T[] t = (T[]) retVal;

        endTransaction();

        return t;
    }

    @Override
    public T save(Object entity) throws DAOException {
        try {
            initTransaction();

            toTrim(entity);

            T t = (T) this.getSession().save(entity);

            endTransaction();

            return t;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            logger.error(e);
            throw new DAOException(e);
        }
    }

    @Override
    public void save(Object... entities) throws DAOException {
        for (Object entity : entities) {
            if (entity != null) {
                save(entity);
            }
        }
    }

    @Override
    public T saveUpper(Object entity) throws DAOException {
        try {
            initTransaction();

            toUpperCaseTrim(entity);

            T t = (T) this.getSession().save(entity);

            endTransaction();

            return t;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            logger.error(e);
            throw new DAOException(e);
        }

    }

    @Override
    public void saveUpper(Object... entities) throws DAOException {
        for (Object entity : entities) {
            if (entity != null) {
                saveUpper(entity);
            }
        }
    }

    @Override
    public void updateUpper(Object entity) throws DAOException {
        try {
            initTransaction();

            toUpperCaseTrim(entity);

            this.getSession().merge(entity);

            endTransaction();

        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            logger.error(e);
            throw new DAOException(e);

        }
    }

    @Override
    public void update(Object entity) throws DAOException {
        try {
            initTransaction();

            toTrim(entity);

            this.getSession().merge(entity);

            endTransaction();
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            logger.error(e);
            throw new DAOException(e);
        }
    }

    @Override
    public boolean remove(Object entity) {
        if (entity != null) {
            initTransaction();

            this.getSession().delete(entity);

            endTransaction();

            return true;
        }
        return false;
    }

    @Override
    public void remove(Object... entities) {
        for (Object entity : entities) {
            if (entity != null) {
                remove(entity);
            }
        }
    }

    @Override
    public boolean removeId(Serializable id) {
        if (id != null) {
            initTransaction();

            Object entity = getSession().get(entityClass, id);
            if (entity != null) {
                remove(entity);
                return true;
            }

            endTransaction();
        }
        return false;
    }

    @Override
    public void removeId(Serializable... ids) {
        initTransaction();

        Criteria c = getSession().createCriteria(entityClass);
        c.add(Restrictions.in("id", ids));
        for (Object entity : c.list()) {
            if (entity != null) {
                remove(entity);
            }
        }

        endTransaction();
    }

    @Override
    public List<T> findAll() {

        initTransaction();

        List<T> list = (List<T>) this.getSession().createCriteria(entityClass).list();

        endTransaction();

        return list;
    }

    @Override
    public boolean isAttached(Object entity) {

        initTransaction();

        boolean val = this.getSession().contains(entity);

        endTransaction();

        return val;
    }

    @Override
    public void refresh(Object... entities) {
        for (Object entity : entities) {
            if (entity != null) {
                initTransaction();

                this.getSession().refresh(entities);

                endTransaction();
            }
        }
    }

    @Override
    public void flush() {
        initTransaction();

        this.getSession().flush();

        endTransaction();
    }

    /**
     * Same as <code>_search(ISearch)</code> except that it uses the specified
     * search class instead of getting it from the search object. Also, if the search
     * object has a different search class than what is specified, an exception
     * is thrown.
     */
    @Override
    public List search(ISearch search) {
        if (search == null)
            throw new NullPointerException("Search is null.");
        if (search.getSearchClass() != null && !search.getSearchClass().equals(entityClass))
            throw new IllegalArgumentException("Search class does not match expected type: " + entityClass.getName());

        initTransaction();

        List list = searchProcessor.search(getSession(), entityClass, search);

        endTransaction();

        return list;
    }

    /**
     * Same as <code>_count(ISearch)</code> except that it uses the specified
     * search class instead of getting it from the search object. Also, if the search
     * object has a different search class than what is specified, an exception
     * is thrown.
     */
    @Override
    public int count(ISearch search) {
        if (search == null)
            throw new NullPointerException("Search is null.");
        if (search.getSearchClass() != null && !search.getSearchClass().equals(entityClass))
            throw new IllegalArgumentException("Search class does not match expected type: " + entityClass.getName());

        initTransaction();

        int val = searchProcessor.count(getSession(), entityClass, search);

        endTransaction();

        return val;
    }

    /**
     * Returns the number of instances of this class in the datastore.
     */
    @Override
    public int count() {
        initTransaction();

        List counts = getSession().createQuery("select count(_it_) from " + metadataUtil.get(entityClass).getEntityName() + " _it_").list();

        int sum = 0;

        for (Object count : counts) {
            sum += ((Long) count).intValue();
        }

        endTransaction();
        return sum;
    }

    /**
     * Same as <code>_searchAndCount(ISearch)</code> except that it uses the specified
     * search class instead of getting it from the search object. Also, if the search
     * object has a different search class than what is specified, an exception
     * is thrown.
     */
    @Override
    public SearchResult searchAndCount(ISearch search) {
        if (search == null)
            throw new NullPointerException("Search is null.");
        if (search.getSearchClass() != null && !search.getSearchClass().equals(entityClass))
            throw new IllegalArgumentException("Search class does not match expected type: " + entityClass.getName());

        initTransaction();

        SearchResult searchResult = searchProcessor.searchAndCount(getSession(), entityClass, search);

        endTransaction();

        return searchResult;
    }

    @Override
    public SearchResult lazySearch(int first, int pageSize) {
        List<T> list = null;
        int totalCount = 0;
        SearchResult searchResult = new SearchResult();
        Search search = new Search(this.entityClass);

        initTransaction();

        totalCount = searchProcessor.count(getSession(), search);

        endTransaction();

        searchResult.setTotalCount(totalCount);

        if (totalCount > 0) {
            search.setFirstResult(first);
            search.setMaxResults(pageSize);

            initTransaction();

            list = searchProcessor.search(getSession(), search);

            endTransaction();
        }

        if (list == null) {
            list = new ArrayList<T>();
        }

        searchResult.setResult(list);

        return searchResult;
    }

    @Override
    public SearchResult lazySearch(int first, int pageSize,
                                   String sortField, String sortOrder,
                                   Map<String, Object> filters) {

        List<T> list = null;
        int totalCount = 0;
        SearchResult searchResult = new SearchResult();
        Search search = new Search(this.entityClass);

        if (filters != null && filters.isEmpty() == false) {
            for (Iterator<String> it = filters.keySet().iterator(); it.hasNext(); ) {
                String filterProperty = it.next();
                Object filterValue = filters.get(filterProperty);

                if (filterValue instanceof String) {
                    search.addFilterILike(filterProperty, "%" + (filterValue.toString()) + "%");
                } else if (filterValue instanceof String[]) {
                    String[] arrayFilterValue = (String[]) filterValue;
                    List<String> listFilterValue = Arrays.asList(arrayFilterValue);
                    search.addFilterIn(filterProperty, listFilterValue);
                } else {
                    search.addFilterEqual(filterProperty, filterValue);
                }
            }
        }

        initTransaction();

        totalCount = searchProcessor.count(getSession(), search);

        endTransaction();

        searchResult.setTotalCount(totalCount);

        if (totalCount > 0) {
            if (StringUtils.isBlank(sortField) == false
                    && StringUtils.isBlank(sortOrder) == false) {
                if (sortOrder.toLowerCase().contains("asc")) {
                    search.addSortAsc(sortField);
                }
            }
            search.setFirstResult(first);
            search.setMaxResults(pageSize);

            initTransaction();

            list = searchProcessor.search(getSession(), search);

            endTransaction();
        }

        if (list == null) {
            list = new ArrayList<T>();
        }

        searchResult.setResult(list);

        return searchResult;
    }

    /**
     * Same as <code>_searchUnique(ISearch)</code> except that it uses the specified
     * search class instead of getting it from the search object. Also, if the search
     * object has a different search class than what is specified, an exception
     * is thrown.
     */
    @Override
    public Object searchUnique(ISearch search) {
        if (search == null)
            throw new NullPointerException("Search is null.");
        if (search.getSearchClass() != null && !search.getSearchClass().equals(entityClass))
            throw new IllegalArgumentException("Search class does not match expected type: " + entityClass.getName());

        initTransaction();

        Object obj = searchProcessor.searchUnique(getSession(), entityClass, search);

        endTransaction();

        return obj;
    }

    private void toUpperCaseTrim(Object entity) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        /*Eliminando especios en blanco y pasando a mayúsculas*/
        Field[] fields = entity.getClass().getDeclaredFields();
        String methodList = getMethodsNameByEntity(entity);

        if (methodList == null) {
            return;
        }

        for (Field f : fields) {
            String field = String.valueOf(f.getName().charAt(0)).toUpperCase() + f.getName().substring(1);

            if (f.getType() == String.class
                    && methodList.contains("get" + field)
                    && methodList.contains("set" + field)) {

                Method getter = entity.getClass().getDeclaredMethod("get" + field);
                Method setter = entity.getClass().getDeclaredMethod("set" + field, new Class[]{String.class});

                Object value = getter.invoke(entity, new Object[0]);

                if (value != null) {
                    setter.invoke(entity, ((String) value).trim().toUpperCase());
                }
            }
        }
        /*Fin Eliminando especios en blanco y pasando a mayúsculas*/

    }

    private void toTrim(Object entity) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        /*Eliminando especios en blanco*/
        Field[] fields = entity.getClass().getDeclaredFields();
        String methodList = getMethodsNameByEntity(entity);

        if (methodList.isEmpty()) {
            return;
        }

        for (Field f : fields) {
            String field = String.valueOf(f.getName().charAt(0)).toUpperCase() + f.getName().substring(1);

            if (f.getType() == String.class
                    && methodList.contains("get" + field)
                    && methodList.contains("set" + field)) {

                Method getter = entity.getClass().getDeclaredMethod("get" + field);
                Method setter = entity.getClass().getDeclaredMethod("set" + field, new Class[]{String.class});

                Object value = getter.invoke(entity, new Object[0]);

                if (value != null) {
                    setter.invoke(entity, ((String) value).trim());
                }
            }
        }
        /*Fin Eliminando especios en blanco*/
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private String getMethodsNameByEntity(Object entity) {

        String methodList = "";
        Method[] methods = entity.getClass().getDeclaredMethods();

        if (methods != null) {
            methodList = Arrays.toString(methods);
        }

        return methodList;
    }

    private void initTransaction() {
        this.getSession().beginTransaction();
    }

    private void endTransaction() {
        this.getSession().getTransaction().commit();
        this.getSession().close();
    }
}
