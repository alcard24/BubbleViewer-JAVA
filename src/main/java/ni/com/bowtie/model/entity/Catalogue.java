package ni.com.bowtie.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * Bow Tie Product 2017 - 2019
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 2/2/2019 10:46 PM
 */

@Entity
@Table(name = "catalogue", schema = "public")
@SequenceGenerator(name = "catalogue_id_SEQ", sequenceName = "catalogue_id_seq", allocationSize = 1)
public class Catalogue implements Serializable {

    @Id
    @GeneratedValue(generator = "catalogue_id_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "description")
    private String description;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "master", referencedColumnName = "id")
    private Catalogue master;

    @Basic
    @Column(name = "code", unique = true, nullable = true)
    private String code;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Catalogue catalogue = (Catalogue) o;
        return Objects.equals(id, catalogue.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Catalogue getMaster() {
        return master;
    }

    public void setMaster(Catalogue master) {
        this.master = master;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return name;
    }
}
