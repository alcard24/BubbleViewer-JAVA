package ni.com.bowtie.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Fender Josué Mora Calero, fenmora@outlook.es
 * @version 1.0
 * </p>
 * 19-Oct-18 8:55 PM
 */
@Entity
@Table(name = "media", schema = "public")
@SequenceGenerator(name = "media_id_SEQ", sequenceName = "media_id_seq", allocationSize = 1)
public class Media implements Serializable {

    @Id
    @GeneratedValue(generator = "media_id_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Media media = (Media) o;
        return Objects.equals(id, media.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getId() {
        return String.valueOf(id);
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
