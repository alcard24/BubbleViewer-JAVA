package ni.com.bowtie.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/17/2018 11:06 PM
 */


@Entity
@Table(name = "question", schema = "public")
@SequenceGenerator(name = "question_id_SEQ", sequenceName = "question_id_seq", allocationSize = 1)
public class Question implements Serializable {

    @Id
    @GeneratedValue(generator = "question_id_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Basic
    @Column(name = "body")
    private String body;

    @Basic
    @Column(name = "question_val")
    private BigDecimal questionVal;

    @ManyToOne
    @JoinColumn(name = "test", referencedColumnName = "id")
    private Test test;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return Objects.equals(id, question.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return body;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public BigDecimal getQuestionVal() {
        return questionVal;
    }

    public void setQuestionVal(BigDecimal questionVal) {
        this.questionVal = questionVal;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }
}
