package ni.com.bowtie.model.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
@Entity
@Table(name = "test", schema = "public")
@SequenceGenerator(name = "test_id_SEQ", sequenceName = "test_id_seq", allocationSize = 1)
public class Test implements Serializable {

    @Id
    @GeneratedValue(generator = "test_id_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Basic
    @Column(name = "name")
    private String name;

    @Basic
    @Column(name = "subject")
    private String subject;

    @Basic
    @Column(name = "created_date")
    private Timestamp createdDate;

    @Basic
    @Column(name = "planification_date")
    private Timestamp planificationDate;

    @ManyToOne
    @JoinColumn(name = "test_type", referencedColumnName = "id")
    private Catalogue testType;

    @Basic
    @Column(name = "test_val")
    private BigDecimal testVal = BigDecimal.ONE;

    @Basic
    @Column(name = "minimal_aproved_score")
    private BigDecimal minimalApovedScore = BigDecimal.ONE;

    @Basic
    @Column(name = "reviewed")
    private BigDecimal reviewed = BigDecimal.ONE;

    @Basic
    @Column(name = "closed")
    private BigDecimal closed = BigDecimal.ONE;

    @Basic
    @Column(name = "reject")
    private BigDecimal reject = BigDecimal.ONE;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Catalogue getTestType() {
        return testType;
    }

    public void setTestType(Catalogue testType) {
        this.testType = testType;
    }

    public Timestamp getPlanificationDate() {
        return planificationDate;
    }

    public void setPlanificationDate(Timestamp planificationDate) {
        this.planificationDate = planificationDate;
    }

    public BigDecimal getTestVal() {
        return testVal;
    }

    public void setTestVal(BigDecimal testVal) {
        this.testVal = testVal;
    }

    public BigDecimal getMinimalApovedScore() {
        return minimalApovedScore;
    }

    public void setMinimalApovedScore(BigDecimal minimalApovedScore) {
        this.minimalApovedScore = minimalApovedScore;
    }

    public BigDecimal getReviewed() {
        return reviewed;
    }

    public void setReviewed(BigDecimal reviewed) {
        this.reviewed = reviewed;
    }

    public BigDecimal getClosed() {
        return closed;
    }

    public BigDecimal getReject() {
        return reject;
    }

    public void setReject(BigDecimal reject) {
        this.reject = reject;
    }

    public void setClosed(BigDecimal closed) {
        this.closed = closed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Test test = (Test) o;
        return Objects.equals(id, test.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
