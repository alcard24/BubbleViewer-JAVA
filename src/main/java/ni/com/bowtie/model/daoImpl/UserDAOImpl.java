package ni.com.bowtie.model.daoImpl;

import ni.com.bowtie.model.dao.UserDAO;
import ni.com.bowtie.model.entity.User;
import ni.com.bowtie.model.util.BaseGenericDAOImpl;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/16/2018 10:29 PM
 */
public class UserDAOImpl extends BaseGenericDAOImpl<User, Integer> implements UserDAO {

}
