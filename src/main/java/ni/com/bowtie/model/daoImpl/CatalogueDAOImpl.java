package ni.com.bowtie.model.daoImpl;

import ni.com.bowtie.model.dao.CatalogueDAO;
import ni.com.bowtie.model.entity.Catalogue;
import ni.com.bowtie.model.util.BaseGenericDAOImpl;

/**
 * <p>
 * Bow Tie Product 2017 - 2019
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 2/2/2019 10:51 PM
 */
public class CatalogueDAOImpl extends BaseGenericDAOImpl<Catalogue, Integer> implements CatalogueDAO {
}
