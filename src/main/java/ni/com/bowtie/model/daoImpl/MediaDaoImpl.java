package ni.com.bowtie.model.daoImpl;

import ni.com.bowtie.model.dao.MediaDAO;
import ni.com.bowtie.model.entity.Media;
import ni.com.bowtie.model.util.BaseGenericDAOImpl;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Fender Josué Mora Calero, fenmora@outlook.es
 * @version 1.0
 * </p>
 * 19-Oct-18 9:43 PM
 */
public class MediaDaoImpl extends BaseGenericDAOImpl<Media,Integer> implements MediaDAO {
}
