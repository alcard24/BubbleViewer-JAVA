package ni.com.bowtie.model.daoImpl;

import ni.com.bowtie.model.dao.AnswerDAO;
import ni.com.bowtie.model.entity.Answer;
import ni.com.bowtie.model.util.BaseGenericDAOImpl;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/18/2018 12:10 AM
 */
public class AnswerDAOImpl extends BaseGenericDAOImpl<Answer, Integer> implements AnswerDAO {
}
