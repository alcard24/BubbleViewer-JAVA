package ni.com.bowtie.businessLogic.service;

import ni.com.bowtie.model.entity.Catalogue;
import ni.com.bowtie.model.entity.Test;
import ni.com.bowtie.model.util.DAOException;
import ni.com.bowtie.view.create.OrderByMain;

import java.sql.Timestamp;
import java.util.List;

/**
 *
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 *
 */
public interface TestService {

    /**
     * Save Test
     *
     * @param test, Test Object
     * @throws DAOException, Exception
     */
    void save(Test test) throws DAOException;

    /**
     * Find By Filter Paged
     *
     * @param testType, Test Type
     * @param name,     Name
     * @param fromDate, From Date
     * @param toDate,   To Date
     * @param orderBy,  Order By
     * @param initRow,  Init Row
     * @param maxRows,  Max Rows
     * @return List<Test>
     */
    List<Test> findByFilterPaged(Catalogue testType, String name, Timestamp fromDate, Timestamp toDate, OrderByMain orderBy, int initRow, int maxRows);

    /**
     * Find Total By Filter
     *
     * @param testType, Test Type
     * @param name,     Name
     * @param fromDate, From Date
     * @param toDate,   To Date
     * @return Integer
     */
    Integer findTotalByFilter(Catalogue testType, String name, Timestamp fromDate, Timestamp toDate);

    /**
     * Find All
     *
     * @return List<Test>
     */
    List<Test> findAll();
}
