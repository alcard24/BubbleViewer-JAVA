package ni.com.bowtie.businessLogic.service;

import ni.com.bowtie.model.entity.Catalogue;

import java.util.List;

/**
 * <p>
 * Bow Tie Product 2017 - 2019
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 2/2/2019 10:53 PM
 */
public interface CatalogueService {

    /**
     * Find All By Master Code
     *
     * @param code, Catalogue Master Code
     * @return List
     */
    List<Catalogue> findAllByMasterCode(String code);

    Catalogue findByCode(String code);

}
