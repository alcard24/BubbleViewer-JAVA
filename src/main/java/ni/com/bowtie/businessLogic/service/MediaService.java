package ni.com.bowtie.businessLogic.service;

import ni.com.bowtie.model.entity.Media;
import ni.com.bowtie.model.util.DAOException;

import java.util.List;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Fender Josué Mora Calero, fenmora@outlook.es
 * @version 1.0
 * </p>
 * 19-Oct-18 9:03 PM
 */
public interface MediaService {
    /**
     *
     * @param media, Media Object
     * @throws DAOException, Exception
     */
    void save(Media media) throws DAOException;

    /**
     *
     * @param media, Media
     * @throws DAOException, Exception
     */
    void updateMedia(Media media) throws DAOException;

    /**
     *
     * @return List<Media>
     */
    List<Media> findAllMediaById( Media media);
}
