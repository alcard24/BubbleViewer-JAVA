package ni.com.bowtie.businessLogic.service;

import ni.com.bowtie.model.entity.User;
import ni.com.bowtie.model.util.DAOException;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/16/2018 11:21 PM
 */
public interface UserService {

    /**
     * Login
     *
     * @param userName,        User Name
     * @param password,        Password
     * @param encryptPassword, Encrypt Password
     * @return User
     */
    User login(String userName, String password, boolean encryptPassword);

    /**
     * Save
     *
     * @param user, user
     */
    void save(User user) throws DAOException;
}
