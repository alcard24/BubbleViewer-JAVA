package ni.com.bowtie.businessLogic.service;

import ni.com.bowtie.model.entity.Question;
import ni.com.bowtie.model.entity.Test;
import ni.com.bowtie.model.util.DAOException;

import java.util.List;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/18/2018 12:07 AM
 */
public interface QuestionService {

    /**
     * Update Question
     *
     * @param question, Question
     */
    void updateQuestion(Question question) throws DAOException;

    /**
     * Save Question
     *
     * @param question, Question
     */
    void saveQuestion(Question question) throws DAOException;

    /**
     * Find All Question By Test
     *
     * @param test, Test
     * @return List<Question>
     */
    List<Question> findAllQuestionByTest(Test test);
}
