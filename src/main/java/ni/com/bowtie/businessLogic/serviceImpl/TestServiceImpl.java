package ni.com.bowtie.businessLogic.serviceImpl;

import com.googlecode.genericdao.search.Search;
import ni.com.bowtie.businessLogic.service.TestService;
import ni.com.bowtie.model.dao.TestDAO;
import ni.com.bowtie.model.daoImpl.TestDAOImpl;
import ni.com.bowtie.model.entity.Catalogue;
import ni.com.bowtie.model.entity.Test;
import ni.com.bowtie.model.util.DAOException;
import ni.com.bowtie.view.create.OrderByMain;
import org.apache.commons.lang3.StringUtils;

import java.sql.Timestamp;
import java.util.List;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * <p>
 * 9/4/2018 6:59 PM
 */
public class TestServiceImpl implements TestService {

    private TestDAO testDAO = new TestDAOImpl();

    @Override
    public void save(Test test) throws DAOException {

        testDAO.save(test);

    }

    @Override
    public List<Test> findByFilterPaged(Catalogue testType, String name, Timestamp fromDate, Timestamp toDate, OrderByMain orderByMain, int initRow, int maxRows) {
        Search search = new Search();

        if (testType != null) {
            search.addFilterEqual("testType.id", testType.getId());
        }
        if (!StringUtils.isBlank(name)) {
            search.addFilterLike("name", "%".concat(name).concat("%"));
        }

        if (fromDate != null && toDate != null) {
            search.addFilterGreaterOrEqual("createdDate", fromDate);
            search.addFilterLessOrEqual("createdDate", toDate);
        }

        switch (orderByMain) {
            case TEST_TYPE:
                search.addSort("testType.name", false, true);
                break;
            case NAME:
                search.addSort("name", false, true);
                break;
            case DATE:
                search.addSort("createdDate", true);
                break;
        }

        search.setPage(initRow);
        search.setMaxResults(maxRows);

        return testDAO.search(search);
    }

    @Override
    public Integer findTotalByFilter(Catalogue testType, String name, Timestamp fromDate, Timestamp toDate) {
        Search search = new Search();

        if (testType != null) {
            search.addFilterEqual("master.id", testType.getId());
        }
        if (!StringUtils.isBlank(name)) {
            search.addFilterEqual("name", name);
        }

        if (fromDate != null && toDate != null) {
            search.addFilterGreaterOrEqual("createdDate", fromDate);
            search.addFilterLessOrEqual("createdDate", toDate);
        }

        return testDAO.count(search);
    }

    @Override
    public List<Test> findAll() {
        return testDAO.findAll();
    }
}
