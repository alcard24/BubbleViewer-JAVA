package ni.com.bowtie.businessLogic.serviceImpl;

import com.googlecode.genericdao.search.Search;
import ni.com.bowtie.businessLogic.service.QuestionService;
import ni.com.bowtie.model.dao.QuestionDAO;
import ni.com.bowtie.model.daoImpl.QuestionDAOImpl;
import ni.com.bowtie.model.entity.Question;
import ni.com.bowtie.model.entity.Test;
import ni.com.bowtie.model.util.DAOException;

import java.util.List;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/18/2018 12:07 AM
 */
public class QuestionServiceImpl implements QuestionService {

    private QuestionDAO questionDAO = new QuestionDAOImpl();

    @Override
    public void updateQuestion(Question question) throws DAOException {
        questionDAO.update(question);
    }

    @Override
    public void saveQuestion(Question question) throws DAOException {
        questionDAO.save(question);
    }

    @Override
    public List<Question> findAllQuestionByTest(Test test) {
        Search search = new Search();

        search.addFilterEqual("test.id", test.getId());

        return questionDAO.search(search);
    }
}
