package ni.com.bowtie.businessLogic.serviceImpl;

import com.googlecode.genericdao.search.Search;
import ni.com.bowtie.businessLogic.service.MediaService;
import ni.com.bowtie.model.dao.MediaDAO;
import ni.com.bowtie.model.daoImpl.MediaDaoImpl;
import ni.com.bowtie.model.entity.Media;
import ni.com.bowtie.model.util.DAOException;

import java.util.List;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Fender Josué Mora Calero, fenmora@outlook.es
 * @version 1.0
 * </p>
 * 19-Oct-18 9:13 PM
 */
public class MediaServiceImpl implements MediaService {

    private MediaDAO mediaDAO = new MediaDaoImpl();

    @Override
    public void save(Media media) throws DAOException {
        mediaDAO.save(media);
    }

    @Override
    public void updateMedia(Media media) throws DAOException {
        mediaDAO.update(media);
    }

    @Override
    public List<Media> findAllMediaById(Media media) {
        Search search = new Search();
        search.addFilterLike("media.id",media.getId());
        return null;
    }

}
