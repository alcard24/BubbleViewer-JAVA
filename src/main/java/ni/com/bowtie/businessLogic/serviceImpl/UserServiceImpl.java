package ni.com.bowtie.businessLogic.serviceImpl;

import com.googlecode.genericdao.search.Search;
import ni.com.bowtie.businessLogic.service.UserService;
import ni.com.bowtie.model.dao.UserDAO;
import ni.com.bowtie.model.daoImpl.UserDAOImpl;
import ni.com.bowtie.model.entity.User;
import ni.com.bowtie.model.util.DAOException;
import ni.com.bowtie.util.BCryptUtil;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/16/2018 11:21 PM
 */
public class UserServiceImpl implements UserService {

    private UserDAO userDAO = new UserDAOImpl();

    @Override
    public User login(String userName, String password, boolean encryptPassword) {
        String pass = password;

        if (encryptPassword) {
            pass = BCryptUtil.makeHash(password);
        }

        Search search = new Search();

        search.addFilterEqual("userName", userName);
        search.addFilterEqual("password", pass);

        return (User) userDAO.searchUnique(search);
    }

    @Override
    public void save(User user) throws DAOException {
        userDAO.save(user);
    }
}
