package ni.com.bowtie.businessLogic.serviceImpl;

import com.googlecode.genericdao.search.Search;
import ni.com.bowtie.businessLogic.service.CatalogueService;
import ni.com.bowtie.model.dao.CatalogueDAO;
import ni.com.bowtie.model.daoImpl.CatalogueDAOImpl;
import ni.com.bowtie.model.entity.Catalogue;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * Bow Tie Product 2017 - 2019
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 2/2/2019 10:55 PM
 */
public class CatalogueServiceImpl implements CatalogueService {

    private CatalogueDAO catalogueDAO = new CatalogueDAOImpl();

    @Override
    public List<Catalogue> findAllByMasterCode(String code) {

        List<Catalogue> lCatalogues;

        Search search = new Search();

        search.addFilterEqual("master.code", code);


        lCatalogues = catalogueDAO.search(search);

        return lCatalogues;
    }

    @Override
    public Catalogue findByCode(String code) {
        Search search = new Search();

        search.addFilterEqual("code", code);

        return (Catalogue) catalogueDAO.searchUnique(search);
    }


}
