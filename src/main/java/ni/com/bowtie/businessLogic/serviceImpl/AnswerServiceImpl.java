package ni.com.bowtie.businessLogic.serviceImpl;

import com.googlecode.genericdao.search.Search;
import ni.com.bowtie.businessLogic.service.AnswerService;
import ni.com.bowtie.model.dao.AnswerDAO;
import ni.com.bowtie.model.daoImpl.AnswerDAOImpl;
import ni.com.bowtie.model.entity.Answer;
import ni.com.bowtie.model.entity.Question;
import ni.com.bowtie.model.util.DAOException;

import java.util.List;

/**
 * <p>
 * Bow Tie Product 2017 - 2018
 *
 * @author Kevin Alexander Gaitán Argüello, kevinnica02@hotmail.com
 * @version 1.0
 * </p>
 * 10/18/2018 12:07 AM
 */
public class AnswerServiceImpl implements AnswerService {

    private AnswerDAO answerDAO = new AnswerDAOImpl();

    @Override
    public void updateAnswer(Answer answer) throws DAOException {
        answerDAO.update(answer);
    }

    @Override
    public void saveAnswer(Answer answer) throws DAOException {
        answerDAO.save(answer);
    }

    @Override
    public List<Answer> findAllAnswerByQuestion(Question question) {
        Search search = new Search();

        search.addFilterEqual("question.id", question.getId());

        return answerDAO.search(search);
    }
}
